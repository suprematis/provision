package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerConnections)
}

func registerConnections(app *cobra.Command) {
	conn := &cobra.Command{
		Use:   "connections",
		Short: "Retrieve information on API and Websocket connections to dr-provision",
	}

	conn.AddCommand(&cobra.Command{
		Use:   "list",
		Short: "List all connections to the dr-provision API",
		Long:  "Retrieve basic information on all clients connection to dr-provision",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			res := models.ConnectionsList{}
			if err := Session.Req().UrlFor("connections").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	conn.AddCommand(&cobra.Command{
		Use:   "show",
		Short: "Show a single connection to the dr-provision API",
		Long:  "Retrieve basic information on a single connection to dr-provision",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Exactly one argument needed")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := models.Connection{}
			if err := Session.Req().UrlFor("connections", args[0]).Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	app.AddCommand(conn)
}
