package cli

import (
	"fmt"
	"gitlab.com/rackn/provision/v4/api"
	"testing"
)

func ptest(t *testing.T, fnString string, perr error, data []map[string]interface{}, rerr []error, res []bool) {
	t.Helper()

	fq, err := api.ParseAwaitFunctions(fnString)
	if err != nil {
		if perr == nil {
			t.Errorf("Got an error parsing but should not %s: %v", fnString, err)
		} else if perr.Error() != err.Error() {
			t.Errorf("Got unexpected error: (exp) %v != (got) %v", perr, err)
		}
	} else if err == nil && perr != nil {
		t.Errorf("Didn't get a parse error, but expected: %v", perr)
	} else if len(fq) != len(data) {
		t.Errorf("Should have returned %d function only: %d", len(data), len(fq))
	} else {
		for i, testData := range data {
			terr := rerr[i]
			tres := res[i]
			ok, err := fq[0](testData)
			if err != nil {
				if terr == nil {
					t.Errorf("Should not have returned an error: %v", err)
				} else if terr.Error() != err.Error() {
					t.Errorf("Got %d:%v unexpected error: (exp) %v != (got) %v", i, testData, perr, err)
				}
			} else if ok != tres {
				t.Errorf("Response error: %d:%v exp %v != got %v", i, testData, tres, ok)
			}
		}
	}
}

func TestWaitParser(t *testing.T) {
	ptest(t, "And(xxx=Eq({\"test\": 12}),yyy=Eq(\"fred\"))", nil, []map[string]interface{}{
		{
			"xxx": map[string]interface{}{
				"test": 12,
			},
			"yyy": "fred",
		},
	}, []error{nil}, []bool{true})
	ptest(t, "And(xxx=Eq({\"test\": 12}),yyy=Eq(\"fred\"))", nil, []map[string]interface{}{
		{
			"xxx": 13,
			"yyy": "fred",
		},
	}, []error{nil}, []bool{false})

	ptest(t, "x.y.z=Eq(12)", nil, []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"y": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{true})

	ptest(t, "x.y.z=Eq(12)", nil, []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"a": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{false})

	ptest(t, "Not(x.y.z=Eq(12))", nil, []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"a": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{true})

	ptest(t, "Not(x.y=Eq({\"z\": 12}))", nil, []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"y": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{false})

	ptest(t, "Or(k=12,x.y=Eq({\"z\": 12}))", fmt.Errorf("Equals test missing Eq function"), []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"y": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{true})

	ptest(t, "Or(k=Eq(12),x.y=Eq({\"z\": 12}))", nil, []map[string]interface{}{
		{
			"x": map[string]interface{}{
				"y": map[string]interface{}{
					"z": 12,
				},
			},
		},
	}, []error{nil}, []bool{true})
}
