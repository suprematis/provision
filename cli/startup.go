package cli

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	v4 "gitlab.com/rackn/provision/v4"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
)

type registerSection func(*cobra.Command)

var (
	version              = v4.RSVersion
	debug                = false
	catalog              = "https://repo.rackn.io"
	catalogUrlsToFetch   = "all"
	catalogSource        = ""
	defaultCatalog       = "https://repo.rackn.io"
	endpoint             = "https://127.0.0.1:8092"
	defaultEndpoints     = []string{"https://127.0.0.1:8092"}
	token                = ""
	defaultToken         = ""
	username             = "rocketskates"
	defaultUsername      = "rocketskates"
	pass		     = ""
	password             = ""
	defaultPassword      = "r0cketsk8ts"
	downloadProxy        = ""
	defaultDownloadProxy = ""
	format               = ""
	defaultFormat        = "json"
	noColor              = false
	defaultNoColor       = false
	// Format is idx=val1,val2,val3;idx2=val1,val2, ...
	// idx = 0 = json string color
	// idx = 1 = json bool color
	// idx = 2 = json number color
	// idx = 3 = json null color
	// idx = 4 = json key color
	// idx = 5 = header color
	// idx = 6 = border color
	// idx = 7 = value1 color
	// idx = 8 = value2 color
	colorString           = "0=32;1=33;2=36;3=90;4=34,1;5=35,6=95;7=32;8=92"
	defaultColorString    = "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92"
	printFields           = ""
	defaultPrintFields    = ""
	defaultTruncateLength = 40
	truncateLength        = 40
	noHeader              = false
	defaultNoHeader       = false
	// Session is the global client access session
	Session                *api.Client
	noToken                = false
	force                  = false
	ref                    = ""
	defaultRef             = ""
	trace                  = ""
	traceToken             = ""
	defaultUrlProxy        = ""
	urlProxy               = ""
	registrations          = []registerSection{}
	objectErrorsAreFatal   = false
	platform               = ""
	clientCert             = ""
	clientKey              = ""
	caCert                 = ""
	serverVerify           = false
	defaultClientCert      = ""
	defaultClientKey       = ""
	defaultCaCert          = ""
	defaultServerVerify    = false
	ignoreUnixProxy        = false
	defaultIgnoreUnixProxy = false
	forceNewSession        = false
	defaultForceNewSession = false
	defaultProfile         = ""
)

func addRegistrar(rs registerSection) {
	registrations = append(registrations, rs)
}

var ppr = func(c *cobra.Command, a []string) error {
	if (password == "" ) {
		if (pass != "" ) {
			password = pass
		} else {
			password = defaultPassword
		}
	}

	c.SilenceUsage = true
	if Session == nil || forceNewSession {
		epInList := false
		for i := range defaultEndpoints {
			if defaultEndpoints[i] == endpoint {
				epInList = true
				break
			}
		}
		if !epInList {
			l := len(defaultEndpoints)
			defaultEndpoints = append(defaultEndpoints, endpoint)
			defaultEndpoints[0], defaultEndpoints[l] = defaultEndpoints[l], defaultEndpoints[0]
		}
		var sessErr error
		for _, endpoint = range defaultEndpoints {
			endpoint = strings.TrimSuffix(endpoint, "/")
			if clientCert != "" || clientKey != "" {
				if clientCert == "" || clientKey == "" {
					return fmt.Errorf("Error creating Session: Must specify both client Key and Certificate")
				}
				Session, sessErr = api.ClientCertSession(context.Background(), endpoint, clientCert, clientKey, !ignoreUnixProxy, serverVerify, caCert)
			} else if token != "" {
				Session, sessErr = api.TokenSessionProxyServer(endpoint, token, !ignoreUnixProxy, serverVerify, caCert)
			} else {
				home := os.ExpandEnv("${HOME}")
				tPath := os.ExpandEnv("${RS_TOKEN_CACHE}")
				if tPath == "" && home != "" {
					tPath = path.Join(home, ".cache", "drpcli", "tokens")
				}
				tokenFile := path.Join(tPath, "."+username+".token")
				if !noToken && tPath != "" {
					if err := os.MkdirAll(tPath, 0700); err == nil {
						if tokenStr, err := ioutil.ReadFile(tokenFile); err == nil {
							Session, sessErr = api.TokenSessionProxyServer(endpoint, string(tokenStr), !ignoreUnixProxy, serverVerify, caCert)
							if sessErr == nil {
								if _, err := Session.Info(); err == nil {
									Session.Trace(trace)
									Session.TraceToken(traceToken)
									break
								}
							}
						}
					}
				}
				Session, sessErr = api.UserSessionTokenProxyContextServer(context.Background(), endpoint, username, password, !noToken, !ignoreUnixProxy, serverVerify, caCert)
				if !noToken && tPath != "" && sessErr == nil {
					if err := os.MkdirAll(tPath, 700); err == nil {
						tok := &models.UserToken{}
						if err := Session.
							Req().UrlFor("users", username, "token").
							Params("ttl", "7200").Do(&tok); err == nil {
							ioutil.WriteFile(tokenFile, []byte(tok.Token), 0600)
						}
					}
				}
			}
			if sessErr == nil {
				break
			}
		}
		if sessErr != nil {
			return fmt.Errorf("Error creating Session: %v", sessErr)
		}
	}
	// We have a session
	if Session != nil {
		Session.UrlProxy(urlProxy)
		Session.Trace(trace)
		Session.TraceToken(traceToken)
	}
	return nil
}

// NewApp is the app start function
func NewApp() *cobra.Command {
	// Don't use color on windows
	if runtime.GOOS == "windows" {
		defaultNoColor = true
	}
	app := &cobra.Command{
		Use:   "drpcli",
		Short: "A CLI application for interacting with the DigitalRebar Provision API",
		Long: `drpcli is a general-purpose command for interacting with a dr-provision endpoint.
It has several subcommands which have their own help.

It also has several environment variables that control aspects of its operation:

* RS_OBJECT_ERRORS_ARE_FATAL: Have drpcli exit with a non-zero exit
  status if a returned object has an Errors field that is not empty.
  Normally it will only exit with a non-zero exit status when the API
  returns with an error or fatal status code.

* RS_ENDPOINTS: A space-separated list of URLS that drpcli should try to
  communicate with.  The first one that authenticates will be used.

* RS_ENDPOINT: The URL that drpcli should try to communicate.  Ignored if
  RS_ENDPOINTS exists in the environment.
  Default to https://127.0.0.1:8092

* RS_URL_PROXY: The HTTP proxy drpcli should use when communicating with the
  dr-provision endpoint.  It functions like the standard http_proxy
  environment variable.

* RS_TOKEN: The token to use for authentication with the dr-provision
  endpoint.  Overrides RS_KEY.

* RS_CATALOG: The URL to use to fetch the artifact catalog.  All commands
  in the 'drpcli catalog' group of commands use this.
  Defaults to https://repo.rackn.io

* RS_FORMAT: The output format drpcli will use.
  Defaults to json

* RS_PRINT_FIELDS: The fields of an object to display in text or table format.
  Defaults to all of them.

* RS_DOWNLOAD_PROXY: The http proxy to use when downloading bootenv ISO files.
  If this is not set we will look for the https_proxy and http_proxy env vars.

* RS_NO_HEADER: Controls whether to print column headers in text or table
  output mode.

* RS_NO_COLOR: Controls whether output to a terminal should be stripped.

* RS_COLORS: Controls the 8 ANSI colors that should be used in colorized
  output.

* RS_TRUNCATE_LENGTH: The max length of an individual column in text or table
  mode.

* RS_FORCE_NEW_SESSION: Should the client always create a new session

* RS_IGNORE_UNIX_PROXY: Should the client ignore the unix domain socket to the server.

* RS_CLIENT_KEY: The path to a client private key.  Used with RS_CLIENT_CERT.

* RS_CLIENT_CERT: The path to a client certificate.  Used with RS_CLIENT_KEY.

* RS_SERVER_VERIFY: Should the client verify the server's certificate.

* RS_CA_CERT: The path to an additional CA certificate.

* RS_KEY: The default username:password to use when missing a token.`,
	}
	if oeaf := os.Getenv("RS_OBJECT_ERRORS_ARE_FATAL"); oeaf == "true" {
		objectErrorsAreFatal = true
	}
	if dep := os.Getenv("RS_ENDPOINTS"); dep != "" {
		defaultEndpoints = strings.Split(dep, " ")
	}
	if ep := os.Getenv("RS_ENDPOINT"); ep != "" {
		defaultEndpoints = []string{ep}
	}
	if ep := os.Getenv("RS_URL_PROXY"); ep != "" {
		defaultUrlProxy = ep
	}
	if un := os.Getenv("RS_USERNAME"); un != "" {
		defaultUsername = un
	}
	if pa := os.Getenv("RS_=ASSWORD"); pa != "" {
		pass = pa
	}
	if tk := os.Getenv("RS_TOKEN"); tk != "" {
		defaultToken = tk
	}
	if tk := os.Getenv("RS_CLIENT_CERT"); tk != "" {
		defaultClientCert = tk
	}
	if tk := os.Getenv("RS_CLIENT_KEY"); tk != "" {
		defaultClientKey = tk
	}
	if tk := os.Getenv("RS_CA_CERT"); tk != "" {
		defaultCaCert = tk
	}
	if tk := os.Getenv("RS_SERVER_VERIFY"); tk != "" {
		var e error
		defaultServerVerify, e = strconv.ParseBool(tk)
		if e != nil {
			log.Fatal("RS_SERVER_VERIFY should be a boolean value")
		}
	}
	if tk := os.Getenv("RS_IGNORE_UNIX_PROXY"); tk != "" {
		var e error
		defaultIgnoreUnixProxy, e = strconv.ParseBool(tk)
		if e != nil {
			log.Fatal("RS_IGNORE_UNIX_PROXY should be a boolean value")
		}
	}
	if tk := os.Getenv("RS_FORCE_NEW_SESSION"); tk != "" {
		var e error
		defaultForceNewSession, e = strconv.ParseBool(tk)
		if e != nil {
			log.Fatal("RS_FORCE_NEW_SESSION should be a boolean value")
		}
	}
	if tk := os.Getenv("RS_CATALOG"); tk != "" {
		defaultCatalog = tk
	}
	if tk := os.Getenv("RS_FORMAT"); tk != "" {
		defaultFormat = tk
	}
	if tk := os.Getenv("RS_PRINT_FIELDS"); tk != "" {
		defaultPrintFields = tk
	}
	if tk := os.Getenv("RS_DOWNLOAD_PROXY"); tk != "" {
		defaultDownloadProxy = tk
	} else if tk := os.Getenv("https_proxy"); tk != "" {
		defaultDownloadProxy = tk
	} else if tk := os.Getenv("HTTPS_PROXY"); tk != "" {
		defaultDownloadProxy = tk
	} else if tk := os.Getenv("http_proxy"); tk != "" {
		defaultDownloadProxy = tk
	} else if tk := os.Getenv("HTTP_PROXY"); tk != "" {
		defaultDownloadProxy = tk
	}
	if tk := os.Getenv("RS_NO_HEADER"); tk != "" {
		var e error
		defaultNoHeader, e = strconv.ParseBool(tk)
		if e != nil {
			log.Fatal("RS_NO_HEADER should be a boolean value")
		}
	}
	if tk := os.Getenv("RS_NO_COLOR"); tk != "" {
		var e error
		defaultNoColor, e = strconv.ParseBool(tk)
		if e != nil {
			log.Fatal("RS_NO_COLOR should be a boolean value")
		}
	}
	if tk := os.Getenv("RS_COLORS"); tk != "" {
		defaultColorString = tk
	}
	if tk := os.Getenv("RS_TRUNCATE_LENGTH"); tk != "" {
		var e error
		defaultTruncateLength, e = strconv.Atoi(tk)
		if e != nil {
			log.Fatal("RS_TRUNCATE_LENGTH should be an integer value")
		}
	}
	if kv := os.Getenv("RS_KEY"); kv != "" {
		key := strings.SplitN(kv, ":", 2)
		if len(key) < 2 {
			log.Fatal("RS_KEY does not contain a username:password pair!")
		}
		if key[0] == "" || key[1] == "" {
			log.Fatal("RS_KEY contains an invalid username:password pair!")
		}
		defaultUsername = key[0]
		pass = key[1]
	}

	home := getHome()

	if data, err := ioutil.ReadFile(fmt.Sprintf("%s/.drpclirc", home)); err == nil {
		lines := strings.Split(string(data), "\n")
		for _, line := range lines {
			line = strings.TrimSpace(line)
			parts := strings.SplitN(line, "=", 2)

			switch parts[0] {
			case "RS_NO_HEADER":
				var e error
				defaultNoHeader, e = strconv.ParseBool(parts[1])
				if e != nil {
					log.Fatal("RS_NO_HEADER should be a boolean value in drpclirc")
				}
			case "RS_NO_COLOR":
				var e error
				defaultNoColor, e = strconv.ParseBool(parts[1])
				if e != nil {
					log.Fatal("RS_NO_HEADER should be a boolean value in drpclirc")
				}
			case "RS_COLORS":
				defaultColorString = parts[1]
			case "RS_ENDPOINT":
				defaultEndpoints = []string{parts[1]}
			case "RS_ENDPOINTS":
				defaultEndpoints = strings.Split(parts[1], " ")
			case "RS_TOKEN":
				defaultToken = parts[1]
			case "RS_USERNAME":
				defaultUsername = parts[1]
			case "RS_PASSWORD":
				pass = parts[1]
			case "RS_URL_PROXY":
				defaultUrlProxy = parts[1]
			case "RS_DOWNLOAD_PROXY":
				defaultDownloadProxy = parts[1]
			case "RS_FORMAT":
				defaultFormat = parts[1]
			case "RS_PRINT_FIELDS":
				defaultPrintFields = parts[1]
			case "RS_CA_CERT":
				defaultCaCert = parts[1]
			case "RS_CLIENT_KEY":
				defaultClientKey = parts[1]
			case "RS_CLIENT_CERT":
				defaultClientCert = parts[1]
			case "RS_PROFILE":
				defaultProfile = parts[1]
			case "RS_SERVER_VERIFY":
				var e error
				defaultServerVerify, e = strconv.ParseBool(parts[1])
				if e != nil {
					log.Fatal("RS_SERVER_VERIFY should be a boolean value in drpclirc")
				}
			case "RS_IGNORE_UNIX_PROXY":
				var e error
				defaultIgnoreUnixProxy, e = strconv.ParseBool(parts[1])
				if e != nil {
					log.Fatal("RS_IGNORE_UNIX_PROXY should be a boolean value in drpclirc")
				}
			case "RS_FORCE_NEW_SESSION":
				var e error
				defaultForceNewSession, e = strconv.ParseBool(parts[1])
				if e != nil {
					log.Fatal("RS_FORCE_NEW_SESSION should be a boolean value in drpclirc")
				}
			case "RS_TRUNCATE_LENGTH":
				var e error
				defaultTruncateLength, e = strconv.Atoi(parts[1])
				if e != nil {
					log.Fatal("RS_TRUNCATE_LENGTH should be an integer value in drpclirc")
				}
			case "RS_KEY":
				key := strings.SplitN(parts[1], ":", 2)
				if len(key) < 2 {
					continue
				}
				if key[0] == "" || key[1] == "" {
					continue
				}
				defaultUsername = key[0]
				pass = key[1]
			}
		}
	}
	app.PersistentFlags().StringVarP(&endpoint,
		"endpoint", "E", defaultEndpoints[0],
		"The Digital Rebar Provision API endpoint to talk to")
	app.PersistentFlags().StringVarP(&username,
		"username", "U", defaultUsername,
		"Name of the Digital Rebar Provision user to talk to")
	app.PersistentFlags().StringVarP(&password,
		"password", "P", "",
		fmt.Sprintf("password of the Digital Rebar Provision user (default \"%s\")", defaultPassword))
	app.PersistentFlags().StringVarP(&token,
		"token", "T", defaultToken,
		"token of the Digital Rebar Provision access")
	app.PersistentFlags().BoolVarP(&debug,
		"debug", "d", false,
		"Whether the CLI should run in debug mode")
	app.PersistentFlags().BoolVarP(&noColor,
		"no-color", "N", defaultNoColor,
		"Whether the CLI should output colorized strings")
	app.PersistentFlags().StringVarP(&colorString,
		"colors", "C", defaultColorString,
		`The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2...`)
	app.PersistentFlags().StringVarP(&format,
		"format", "F", defaultFormat,
		`The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table"`)
	app.PersistentFlags().StringVarP(&printFields,
		"print-fields", "J", defaultPrintFields,
		`The fields of the object to display in "text" or "table" mode. Comma separated`)
	app.PersistentFlags().BoolVarP(&noHeader,
		"no-header", "H", defaultNoHeader,
		`Should header be shown in "text" or "table" mode`)
	app.PersistentFlags().IntVarP(&truncateLength,
		"truncate-length", "j", defaultTruncateLength,
		`Truncate columns at this length`)
	app.PersistentFlags().BoolVarP(&force,
		"force", "f", false,
		"When needed, attempt to force the operation - used on some update/patch calls")
	app.PersistentFlags().StringVarP(&ref,
		"ref", "r", defaultRef,
		"A reference object for update commands that can be a file name, yaml, or json blob")
	app.PersistentFlags().StringVarP(&trace,
		"trace", "t", "",
		"The log level API requests should be logged at on the server side")
	app.PersistentFlags().StringVarP(&traceToken,
		"trace-token", "Z", "",
		"A token that individual traced requests should report in the server logs")
	app.PersistentFlags().StringVarP(&catalog,
		"catalog", "c", defaultCatalog,
		"The catalog file to use to get product information")
	app.PersistentFlags().StringVarP(&catalogSource,
		"catalog-source", "S", "",
		"A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog")
	app.PersistentFlags().StringVarP(&downloadProxy,
		"download-proxy", "D", defaultDownloadProxy,
		"HTTP Proxy to use for downloading catalog and content")
	app.PersistentFlags().BoolVarP(&noToken,
		"no-token", "x", noToken,
		"Do not use token auth or token cache")
	app.PersistentFlags().BoolVarP(&objectErrorsAreFatal,
		"exit-early", "X", false,
		"Cause drpcli to exit if a command results in an object that has errors")
	app.PersistentFlags().StringVarP(&urlProxy,
		"url-proxy", "u", defaultUrlProxy,
		"URL Proxy for passing actions through another DRP")
	app.PersistentFlags().StringVarP(&platform,
		"platform", "p", platform,
		"Platform to filter details by. Defaults to current system. Format: arch/os")

	app.PersistentFlags().BoolVar(&forceNewSession,
		"force-new-session", defaultForceNewSession,
		"Should the client always create a new session")
	app.PersistentFlags().BoolVar(&ignoreUnixProxy,
		"ignore-unix-proxy", defaultIgnoreUnixProxy,
		"Should the client ignore unix proxies")
	app.PersistentFlags().BoolVar(&serverVerify,
		"server-verify", defaultServerVerify,
		"Should the client verify the server cert")
	app.PersistentFlags().StringVar(&caCert,
		"ca-cert", defaultCaCert,
		"CA certificate used to verify the server certs (with the system set)")
	app.PersistentFlags().StringVar(&clientCert,
		"client-cert", defaultClientCert,
		"Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD")
	app.PersistentFlags().StringVar(&clientKey,
		"client-key", defaultClientKey,
		"Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD")
	app.PersistentFlags().StringVar(&catalogUrlsToFetch,
		"fetch-catalogs", catalogUrlsToFetch,
		"Determines which catalog urls to use to fetch the catalog. If set to `all`, all the catalog urls defined with catalog_url and catalog_urls as well as the default catalog will be used. If set to server, only the catalog urls defined on the server will be used. If set to cli, then the specific url specified with the -c flag will be used. It defaults to `all`. Allowed values `all, `server`, `cli`.")

	// Flags deprecated due to standardizing on all hyphenated form for persistent flags.
	// TODO do the same thing for flags defined by commands
	app.PersistentFlags().StringVar(&traceToken,
		"traceToken", "",
		"A token that individual traced requests should report in the server logs")
	app.PersistentFlags().BoolVar(&noToken,
		"noToken", noToken,
		"Do not use token auth or token cache")
	app.PersistentFlags().BoolVar(&objectErrorsAreFatal,
		"exitEarly", false,
		"Cause drpcli to exit if a command results in an object that has errors")
	app.PersistentFlags().MarkHidden("traceToken")
	app.PersistentFlags().MarkDeprecated("traceToken", "please use --trace-token")
	app.PersistentFlags().MarkHidden("noToken")
	app.PersistentFlags().MarkDeprecated("noToken", "please use --no-token")
	app.PersistentFlags().MarkHidden("exitEarly")
	app.PersistentFlags().MarkDeprecated("exitEarly", "please use --exit-early")
	if runtime.GOOS != "windows" {
		app.AddCommand(&cobra.Command{
			Use:   "proxy [socket]",
			Short: "Run a local UNIX socket proxy for further drpcli commands.  Requires RS_LOCAL_PROXY to be set in the env.",
			RunE: func(c *cobra.Command, args []string) error {
				if len(args) != 1 {
					return fmt.Errorf("No location for the local proxy socket")
				}
				if pl := os.Getenv("RS_LOCAL_PROXY"); pl != "" {
					return fmt.Errorf("Local proxy already running at %s", pl)
				}
				return Session.RunProxy(args[0])
			},
		})
	}

	for _, rs := range registrations {
		rs(app)
	}

	for _, c := range app.Commands() {
		// contents needs some help.
		switch c.Use {
		case "airgap":
			// We do not want to establish a session in airgap unless specified
			for _, sc := range c.Commands() {
				if strings.HasPrefix(sc.Use, "withInternet") {
					sc.PersistentPreRunE = ppr
				}
			}
		case "archive":
			// archive is just straight local file manipulation.
		case "catalog":
			for _, sc := range c.Commands() {
				if !strings.HasPrefix(sc.Use, "copyLocal") {
					sc.PersistentPreRunE = func(c *cobra.Command, a []string) error {
						err := ppr(c, a)
						if err != nil {
							fmt.Fprintf(os.Stderr, "Continuing without a server connection due to: %v\n", err)
						}
						return nil
					}
				}
			}
		case "config":
		case "contents":
			for _, sc := range c.Commands() {
				if !strings.HasPrefix(sc.Use, "bundle") &&
					!strings.HasPrefix(sc.Use, "unbundle") &&
					!strings.HasPrefix(sc.Use, "document") {
					sc.PersistentPreRunE = ppr
				}
			}
		case "preflight":
			for _, sc := range c.Commands() {
				if !strings.HasPrefix(sc.Use, "checkports") {
					sc.PersistentPreRunE = ppr
				}
			}
		case "support":
			for _, sc := range c.Commands() {
				if !strings.HasPrefix(sc.Use, "crash-bundle") {
					sc.PersistentPreRunE = ppr
				}
			}
		case "users":
			for _, sc := range c.Commands() {
				if !strings.HasPrefix(sc.Use, "passwordhash") {
					sc.PersistentPreRunE = ppr
				}
			}
		case "jobs":
			for _, sc := range c.Commands() {
				if strings.HasPrefix(sc.Use, "unpack") {
					continue
				}
				sc.PersistentPreRunE = ppr
			}

		case "labs":

		default:
			c.PersistentPreRunE = ppr
		}
	}

	// top-level commands that do not need PersistentPreRun go here.
	app.AddCommand(&cobra.Command{
		Use:   "version",
		Short: "Digital Rebar Provision CLI Command Version",
		RunE: func(cmd *cobra.Command, args []string) error {
			fmt.Printf("Version: %v\n", version)
			return nil
		},
	})
	app.AddCommand(&cobra.Command{
		Use:   "autocomplete [filename]",
		Short: "Generate CLI Command Bash AutoCompletion File (may require 'bash-completion' pkg be installed)",
		Long:  "Generate a bash autocomplete file as *filename*.\nPlace the generated file in /etc/bash_completion.d or /usr/local/etc/bash_completion.d.\nMay require the 'bash-completion' package is installed to work correctly.",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 || args[0] == "-" {
				app.GenBashCompletion(os.Stdout)
				return nil
			}
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1  argument", cmd.UseLine())
			}
			app.GenBashCompletionFile(args[0])
			return nil
		},
	})

	app.AddCommand(&cobra.Command{
		Use:   "completion [bash|zsh|fish|powershell]",
		Short: "Generate completion script",
		Long: fmt.Sprintf(`To load completions:
  Bash
  ----
    $ source <(%[1]s completion bash)

  To load completions for each session, execute once:
  Linux:
    # %[1]s completion bash > /etc/bash_completion.d/%[1]s
  macOS:
    # %[1]s completion bash > /usr/local/etc/bash_completion.d/%[1]s

  Zsh
  ---
    $ eval $(%[1]s completion zsh)

  If shell completion is not already enabled in your environment,
  you will need to enable it.  You can execute the following once.

    $ echo "autoload -U compinit; compinit" >> ~/.zshrc

  To load completions for each session, execute the following once.

    # %[1]s completion zsh > "${fpath[1]}/_%[1]s"

  Fish
  ----
    $ %[1]s completion fish | source

  To load completions for each session, execute the following once.

    $ %[1]s completion fish > ~/.config/fish/completions/%[1]s.fish

  PowerShell
  ----------
    PS> %[1]s completion powershell | Out-String | Invoke-Expression

  To load completions for every new session, run the following

    PS> %[1]s completion powershell > %[1]s.ps1

  and source this file from your PowerShell profile.
		`, app.Root().Name()),
		DisableFlagsInUseLine: true,
		ValidArgs:             []string{"bash", "zsh", "fish", "powershell"},
		Args:                  cobra.ExactValidArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			switch args[0] {
			case "bash":
				cmd.Root().GenBashCompletion(os.Stdout)
			case "zsh":
				cmd.Root().GenZshCompletion(os.Stdout)
			case "fish":
				cmd.Root().GenFishCompletion(os.Stdout, true)
			case "powershell":
				cmd.Root().GenPowerShellCompletionWithDesc(os.Stdout)
			}
		},
	})

	app.AddCommand(&cobra.Command{
		Use:   "gohai",
		Short: "Get basic system information as a JSON blob",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			return gohai()
		},
	})

	app.AddCommand(&cobra.Command{
		Use:   "fingerprint",
		Short: "Get the machine fingerprint used to determine what machine we are running on",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			w := &models.Whoami{}
			if err := w.Fill(); err != nil {
				return err
			}
			return prettyPrint(w)
		},
	})
	app.AddCommand(agentHandler())

	app.AddCommand(&cobra.Command{
		Use:   "document [directory]",
		Short: "Output the drpcli markdown docs to [directory]",
		Args:  cobra.ExactValidArgs(1),
		RunE: func(c *cobra.Command, args []string) error {
			dir := args[0]
			linkHandler := func(name string) string {
				return name
			}
			filePrepender := func(name string) string {
				return ""
			}
			return doc.GenMarkdownTreeCustom(app, dir, filePrepender, linkHandler)
		},
	})

	return app
}

func ResetDefaults() {
	defaultEndpoints = []string{"https://127.0.0.1:8092"}
}
