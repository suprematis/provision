package cli

import (
	"encoding/json"
	"fmt"
	"github.com/Masterminds/semver"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"runtime"
	"sort"
	"strings"
)

var ignoreMode = os.ModeDir |
	os.ModeNamedPipe |
	os.ModeSocket |
	os.ModeDevice |
	os.ModeCharDevice |
	os.ModeIrregular

var catalogBuilderMetaFileName = "metadata.yaml"

func oneItem(cat *models.Content, name, version string) *models.CatalogItem {
	items := itemsFromCatalog(cat, name)

	// True up version in case a short version was provided
	version = getMatchingVersion(items, version)

	for _, v := range items {
		if v.Version == version || v.ActualVersion == version {
			return v
		}
	}
	return nil
}

// getMatchingVersion returns the version string. Given a list of catalog items, it loops
// through and finds the item with the largest matching version
// So if the input is v4.8 then it will return the largest v4.8.x number
func getMatchingVersion(items map[string]*models.CatalogItem, version string) string {
	if matched, _ := regexp.MatchString(`^v\d+\.\d+$`, version); !matched {
		return version
	}
	ver, _ := semver.NewVersion(version)
	for _, v := range items {
		cv, _ := semver.NewVersion(v.ActualVersion)
		if ver.Major() == cv.Major() && ver.Minor() == cv.Minor() && cv.Compare(ver) > 0 {
			ver, _ = semver.NewVersion(v.ActualVersion)
		}
	}
	return fmt.Sprintf("v%s", ver.String())
}

func getSupportedOsListFromItem(item *models.CatalogItem) []string {
	supportedOses := make([]string, len(item.Shasum256))
	i := 0
	for k, v := range item.Shasum256 {
		if v != "" {
			supportedOses[i] = k
		}
		i++
	}
	return supportedOses
}

func installItem(catalog *models.Content, name, version, arch, tgtos string, replaceWritable bool, inflight map[string]struct{}) error {
	inflight[name] = struct{}{}
	if name == "BasicStore" {
		return nil
	}
	item := oneItem(catalog, name, version)
	if item == nil {
		return fmt.Errorf("install item: %s version %s not in catalog", name, version)
	}
	src, err := urlOrFileAsReadCloser(item.DownloadUrl(arch, tgtos))
	if src != nil {
		defer src.Close()
	}
	if err != nil {
		return fmt.Errorf("Unable to contact source URL for %s: %v", item.Name, err)
	}
	switch item.ContentType {
	case "ContentPackage":
		content := &models.Content{}
		if err := json.NewDecoder(src).Decode(&content); err != nil {
			return fmt.Errorf("Error downloading content bundle %s: %v", item.Name, err)
		}

		prereqs := strings.Split(content.Meta.Prerequisites, ",")
		for _, p := range prereqs {
			p = strings.TrimSpace(p)
			if p == "" {
				continue
			}

			parts := strings.Split(p, ":")
			pname := strings.TrimSpace(parts[0])

			if _, err := Session.GetContentItem(pname); err == nil {
				inflight[pname] = struct{}{}
				continue
			}

			if err := installItem(catalog, pname, version, arch, tgtos, replaceWritable, inflight); err != nil {
				return err
			}
		}
		return doReplaceContent(content, "", replaceWritable)
	case "PluginProvider":
		res := &models.PluginProviderUploadInfo{}
		req := Session.Req().Post(src).UrlFor("plugin_providers", item.Name)
		if replaceWritable {
			req = req.Params("replaceWritable", "true")
		}
		// TODO: One day handle prereqs.  Save to local file, mark executable, get contents, check prereqs
		if err := req.Do(res); err != nil {
			return err
		}
		return prettyPrint(res)
	case "DRP":
		if info, err := Session.PostBlob(src, "system", "upgrade"); err != nil {
			return generateError(err, "Failed to post upgrade of DRP")
		} else {
			return prettyPrint(info)
		}
	case "DRPUX":
		if info, err := Session.PostBlobExplode(src, true, "files", "ux", "drp-ux.zip"); err != nil {
			return generateError(err, "Failed to post upgrade of DRP")
		} else {
			return prettyPrint(info)
		}
	default:
		return fmt.Errorf("Don't know how to install %s of type %s yet", item.Name, item.ContentType)
	}
}

func catalogCommands() *cobra.Command {

	type catItem struct {
		Type            string
		Versions        []string
		SupportedOsList []string
	}

	type catDetails struct {
		Type     string
		Versions []models.CatalogItem
	}

	cmd := &cobra.Command{
		Use:   "catalog",
		Short: "Access commands related to catalog manipulation",
	}
	cmd.AddCommand(&cobra.Command{
		Use:   "show",
		Short: "Show the contents of the current catalog",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}
			return prettyPrint(catalog)
		},
	})
	cmd.AddCommand(&cobra.Command{
		Use:   "items",
		Short: "Show the items available in the catalog",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}

			items := map[string]catItem{}
			for _, v := range itemsFromCatalog(catalog, "") {
				item := &models.CatalogItem{}
				if err := models.Remarshal(v, &item); err != nil {
					continue
				}
				if _, ok := items[item.Name]; !ok {
					items[item.Name] = catItem{Type: item.ContentType, Versions: []string{item.Version}}
				} else {
					cat := items[item.Name]
					cat.Versions = append(cat.Versions, item.Version)
					items[item.Name] = cat
				}
			}
			for k := range items {
				sort.Strings(items[k].Versions)
			}
			return prettyPrint(items)
		},
	})
	itemCmd := &cobra.Command{
		Use:   "item",
		Short: "Commands to act on individual catalog items",
	}
	var arch, tgtos, version string
	itemCmd.PersistentFlags().StringVar(&arch, "arch", runtime.GOARCH, "Architecture of the item to work with when downloading a plugin provider")
	itemCmd.PersistentFlags().StringVar(&tgtos, "os", runtime.GOOS, "OS of the item to work with when downloading a plugin provider")
	itemCmd.PersistentFlags().StringVar(&version, "version", "stable", "Version of the item to work with")
	itemCmd.AddCommand(&cobra.Command{
		Use:   "download [item] (to [file])",
		Short: "Downloads [item] to [file]",
		Long: `Downloads the specified item to the specified file
If to [file] is not specified, it will be downloaded into current directory
and wind up in a file with the same name as the item + the default file extension for the file type.
`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 && len(args) != 3 {
				return fmt.Errorf("item download requires 1 or 2 arguments")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}
			item := oneItem(catalog, args[0], version)
			if item == nil {
				return fmt.Errorf("download item: %s version %s not in catalog", args[0], version)
			}
			d("item found in catalog..")
			target := item.FileName()
			if len(args) == 3 {
				target = args[2]
			}
			mode := os.FileMode(0644)
			if item.ContentType == "PluginProvider" {
				mode = 0755
			}
			d("downloading item from catalog..")
			src, err := urlOrFileAsReadCloser(item.DownloadUrl(arch, tgtos))
			if src != nil {
				defer src.Close()
			}
			if err != nil {
				return fmt.Errorf("unable to contact source URL for %s: %v", item.Name, err)
			}
			fi, err := os.OpenFile(target, os.O_RDWR|os.O_CREATE|os.O_TRUNC, mode)
			if err != nil {
				return fmt.Errorf("unable to create %s: %v", target, err)
			}
			defer fi.Close()
			_, err = io.Copy(fi, src)
			d("item download complete..")
			return err
		},
	})
	replaceWritable := false
	install := &cobra.Command{
		Use:               "install [item]",
		Short:             "Installs [item] from the catalog on the current dr-provision endpoint",
		PersistentPreRunE: ppr,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("item install requires 1 argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}
			info, err := Session.Info()
			if err != nil {
				return fmt.Errorf("Unable to fetch session information to determine endpoint arch and OS")
			}
			arch = info.Arch
			tgtos = info.Os
			err = installItem(catalog, args[0], version, arch, tgtos, replaceWritable, map[string]struct{}{})
			if err != nil {
				return err
			}
			return nil
		},
	}
	install.Flags().BoolVar(&replaceWritable, "replace-writable", false, "Replace identically named writable objects")
	// Flag deprecated due to standardizing on all hyphenated form for persistent flags.
	install.Flags().BoolVar(&replaceWritable, "replaceWritable", false, "Replace identically named writable objects")
	install.Flags().MarkHidden("replaceWritable")
	install.Flags().MarkDeprecated("replaceWritable", "please use --replace-writable")
	itemCmd.AddCommand(install)
	itemCmd.AddCommand(&cobra.Command{
		Use:   "show [item]",
		Short: "Shows available versions for [item]",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("item show requires 1 argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}

			items := map[string]catItem{}
			for _, v := range itemsFromCatalog(catalog, args[0]) {
				item := &models.CatalogItem{}
				if err := models.Remarshal(v, &item); err != nil {
					continue
				}
				if _, ok := items[item.Name]; !ok {
					items[item.Name] = catItem{
						Type:            item.ContentType,
						Versions:        []string{item.Version},
						SupportedOsList: getSupportedOsListFromItem(item),
					}
				} else {
					cat := items[item.Name]
					cat.Versions = append(cat.Versions, item.Version)
					items[item.Name] = cat
				}
			}
			if len(items) == 0 {
				return fmt.Errorf("No item named %s in the catalog", args[0])
			}
			for k := range items {
				sort.Strings(items[k].Versions)
			}
			return prettyPrint(items[args[0]])
		},
	})
	itemCmd.AddCommand(&cobra.Command{
		Use:   "detail [item]",
		Short: "Shows available details of versions for [item]",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("item show requires 1 argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			catalog, err := fetchCatalog()
			if err != nil {
				return err
			}

			items := map[string]catDetails{}
			for _, v := range itemsFromCatalog(catalog, args[0]) {
				item := &models.CatalogItem{}
				if err := models.Remarshal(v, &item); err != nil {
					continue
				}
				if _, ok := items[item.Name]; !ok {
					items[item.Name] = catDetails{Type: item.ContentType, Versions: []models.CatalogItem{*item}}
				} else {
					cat := items[item.Name]
					cat.Versions = append(cat.Versions, *item)
					items[item.Name] = cat
				}
			}
			if len(items) == 0 {
				return fmt.Errorf("No item named %s in the catalog", args[0])
			}

			return prettyPrint(items[args[0]])
		},
	})
	cmd.AddCommand(itemCmd)

	minVersion := ""
	versions := ""
	versionSet := ""
	tip := false
	concurrency := 1
	downloadAll := false
	updateCmd := &cobra.Command{
		Use:   "updateLocal",
		Short: "Update the local catalog from the upstream catalog",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			location := "tftpboot/files"
			return downloadCatalog(location, minVersion, versions, versionSet, concurrency, downloadAll, false, tip)
		},
	}
	updateCmd.PersistentFlags().StringVar(&minVersion, "version", "stable", "Minimum version of the items. If set to 'stable' will only get stable entries.")
	updateCmd.PersistentFlags().StringVar(&versions, "versions", "", "A comma-separated list of versions to download. NOTE: If this list is provided, the --tip and --version flags will be ignored.")
	updateCmd.PersistentFlags().BoolVar(&tip, "tip", false, "Include tip versions of the packages")
	updateCmd.PersistentFlags().BoolVar(&downloadAll, "all", false, "Download all items from the catalog. This will ignore the version and tip flags.")
	updateCmd.PersistentFlags().IntVar(&concurrency, "concurrency", 1, "Number of concurrent download options")
	updateCmd.PersistentFlags().StringVar(&versionSet, "version-set", "", "A pre-defined set of versions")
	cmd.AddCommand(updateCmd)

	// Download all catalog items to a local location
	location := ""
	minVersion = ""
	versions = ""
	versionSet = ""
	tip = false
	concurrency = 1
	downloadAll = false
	copyCmd := &cobra.Command{
		Use:   "copyLocal",
		Short: "Download all the items in the catalog to a specified location",
		Long: `Downloads all the items in the catalog to the specified location.
If no [location] is specified, it will be downloaded into current directory under /rebar-catalog.`,
		Args: cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			return downloadCatalog(location, minVersion, versions, versionSet, concurrency, downloadAll, true, tip)
		},
	}
	copyCmd.PersistentFlags().StringVar(&location, "location", "", "The location to which the catalog contents need to be downloaded.")
	copyCmd.PersistentFlags().StringVar(&minVersion, "version", "stable", "Minimum version of the items. If set to 'stable' will only get stable entries.")
	copyCmd.PersistentFlags().StringVar(&versions, "versions", "", "A comma-separated list of versions to download. NOTE: If this list is provided, the --tip and --version flags will be ignored.")
	copyCmd.PersistentFlags().BoolVar(&tip, "tip", false, "Include tip versions of the packages")
	copyCmd.PersistentFlags().BoolVar(&downloadAll, "all", false, "Download all items from the catalog. This will ignore the version and tip flags.")
	copyCmd.PersistentFlags().IntVar(&concurrency, "concurrency", 1, "Number of concurrent download options")
	copyCmd.PersistentFlags().StringVar(&versionSet, "version-set", "", "A pre-defined set of versions")
	cmd.AddCommand(copyCmd)

	// Start of create stuff
	var pkgVer string
	createCmd := &cobra.Command{
		Use:   "create",
		Short: "Create a custom catalog for Digital Rebar",
		RunE: func(cmd *cobra.Command, args []string) error {
			cat, err := fetchCatalog()
			if err != nil {
				return err
			}
			newMap := map[string]interface{}{}
			for k, v := range cat.Sections["catalog_items"] {
				item := &models.CatalogItem{}
				if err := models.Remarshal(v, &item); err != nil {
					continue
				}
				if item.Version == pkgVer {
					newMap[k] = item
				}
			}
			cat.Sections["catalog_items"] = newMap
			return prettyPrint(cat)
		},
	}
	createCmd.Flags().StringVarP(&pkgVer, "pkg-version", "", "", "pkg-version tip|stable (required)")
	createCmd.MarkFlagRequired("pkg-version")
	cmd.AddCommand(createCmd)

	// Build catalog command
	catalogName := ""
	fileUrl := ""
	baseDir := ""
	buildCmd := &cobra.Command{
		Use:   "build",
		Short: "Builds the local catalog as per the input and YAML provided. It will automatically look for a metadata.yaml file in the current location",
		Long: `Builds a local catalog with the name specified by catalog-name to a location specified by base-dir. It will also automatically look for a 
a an optional file called metadata.yaml (structure: ContentMetaData from https://gitlab.com/rackn/provision/-/blob/v4/models/content.go) 
and if it exists will update content metadata accordingly.`,
		Args: cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			metadata := models.ContentMetaData{}
			if _, err := os.Stat(catalogBuilderMetaFileName); err == nil {
				buf, err := ioutil.ReadFile(catalogBuilderMetaFileName)
				if err != nil {
					return fmt.Errorf("failed to open file %s: %v", catalogBuilderMetaFileName, err)
				}

				if err := api.DecodeYaml(buf, &metadata); err != nil {
					return fmt.Errorf("failed to unmarshal file metadata.yaml: %v", err)
				}
			}

			input := &models.CatalogBuilderInput{
				FileUrl:     fileUrl,
				BaseDir:     baseDir,
				CatalogName: catalogName,
				Meta:        metadata,
			}
			if _, err := api.BuildCatalog(input); err != nil {
				return fmt.Errorf("failed to build catalog: %v", err)
			}
			return nil
		},
	}
	buildCmd.PersistentFlags().StringVar(&catalogName, "catalog-name", "", "The name of the catalog file being created. Do not include extension. For example rackn-catalog. This would created a file call rackn-catalog.json in the $base-dir/rackn-catalog location.")
	buildCmd.PersistentFlags().StringVar(&fileUrl, "file-url", "", "The url which determines the values of Source and CodeSource in the meta section of the catalog.")
	buildCmd.PersistentFlags().StringVar(&baseDir, "base-dir", "", "The location where the catalog will be built.")
	cmd.AddCommand(buildCmd)

	return cmd
}

func init() {
	addRegistrar(func(c *cobra.Command) { c.AddCommand(catalogCommands()) })
}

func installOne() {

}
