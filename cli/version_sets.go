package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerVersionSet)
}

func registerVersionSet(app *cobra.Command) {
	op := &ops{
		name:       "version_sets",
		singleName: "version_set",
		example:    func() models.Model { return &models.VersionSet{} },
	}
	op.command(app)
}
