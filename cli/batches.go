package cli

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerBatch)
}

func registerBatch(app *cobra.Command) {
	op := &ops{
		name:       "batches",
		singleName: "batch",
		example:    func() models.Model { return &models.Batch{} },
	}
	rerun := &cobra.Command{
		Use:   "rerun [uuid]",
		Short: "Rerun batch",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Must provide a batch")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			id := args[0]
			res := &models.Batch{}
			if err := Session.Req().UrlFor("batches", id).Do(&res); err != nil {
				return fmt.Errorf("Failed to get batch for %s: %v", id, err)
			}
			res.Uuid = nil
			res.SetupWorkOrder = nil
			res.PostWorkOrder = nil
			res.State = ""
			res.StartTime = time.Unix(0, 0)
			res.EndTime = time.Unix(0, 0)
			if err := Session.Req().Post(res).UrlFor("batches").Do(&res); err != nil {
				return fmt.Errorf("Failed to create new batch from %s: %v", id, err)
			}
			return prettyPrint(res)
		},
	}
	op.addCommand(rerun)
	op.command(app)
}
