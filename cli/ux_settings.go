package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerUxSetting)
}

func registerUxSetting(app *cobra.Command) {
	op := &ops{
		name:       "ux_settings",
		singleName: "ux_setting",
		example:    func() models.Model { return &models.UxSetting{} },
	}
	op.command(app)
}
