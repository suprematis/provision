#!/usr/bin/env bash

###
#  Converts the DRP Endpoint 'BackingStore' (read/write objects) to an
#  on-disk set of object files which can be bundled up and maintained in
#  a DRP Content Bundle.
#
#  This script assumes you have 'drpcli' installed on the local system, along
#  with 'wget' or 'curl'.  It will also download a copy of the 'yq' binary
#  from the Github mikefarah/yq project; appropriate for the Platform and
#  Architecture of the system you are running the script on; if the 'yq'
#  binary is not found in the PATH.
#
#  Once complete, YOU MUST EDIT the 'meta.yaml' and provide appropriate
#  Meta data fields for the content bundle.  To use this, 'cd' into the
#  'rw-content' directory (edit the meta.yaml), and do:
#
#    * drpcli contents bundle ../my-content.yaml Version=v1.0.0
#
#  Replace the 'v1.0.0' with an appropriate version.  To install, you must
#  remove/replace all Backing read/write Objects in the DRP Endpoint with
#  the newly installed Content Bundle versions.  This can be done as
#  follows:
#
#    * drpcli contents upload ../my-content.yaml --replace-writable --key=../secrets-key.txt
#
#  The 'drpcli' command must have access to the DRP Endpoint API port, and
#  the $HOME/.drpclirc file or appropriate 'RS_*' environment variables
#  must be set for this script to work.
#
#  NOTICE:  Secure Param handling requires the use of the Secret Key to
#           upload/manage content bundles with encrypted Secure Params.
#           DO NOT LOSE THIS KEY - it is critical for use with Secure
#           Param operations.
###
set -e

function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "!!! FATAL: $*\n"; exit $XIT; }

RUNTIME_OBJECTS="activities alerts jobs leases preferences plugin_providers users work_orders"
FILTER="del(.Bundle)|del(.Endpoint)|del(.Errors)|del(.Partial)|del(.Available)|del(.Validated)|del(.ReadOnly)"

PLAT=$(uname -s | tr '[:upper:]' '[:lower:]')
ARCH=$(uname -p)
[[ "$ARCH" == "x86_64" ]] && ARCH="amd64" || true

if ! which drpcli > /dev/null 2>&1
then
  xiterr 1 "No 'drpcli' in PATH"
fi

if which wget > /dev/null 2>&1
then
  GET="wget -O yq --quiet"
elif which curl > /dev/null 2>&1
then
  GET="curl -fsSL -o yq"
else
  xiterr 1 "No 'wget' or 'curl' found in PATH"
fi

YQ=yq
if ! which yq > /dev/null 2>&1
then
  echo "Getting yq"
  $GET https://github.com/mikefarah/yq/releases/latest/download/yq_${PLAT}_${ARCH}
  chmod +x ./yq
  YQ=`pwd`/yq
fi

if ! ${YQ} -V > /dev/null 2>&1
then
  xiterr 1 "No working 'yq' found or downloaded"
fi

echo "Cleaning up from previous script invocations..."
rm -rf rw-content backing-store.yaml secrets-key.txt

echo "Getting BackingStore (read/write objects) contents..."
drpcli contents show BackingStore --key=secrets-key.txt --format=yaml > backing-store.yaml

mkdir rw-content
cd rw-content
echo "Unpacking contents..."
drpcli contents unbundle ../backing-store.yaml --format=yaml

echo "Removing runtime objects ($RUNTIME_OBJECTS)..."
rm -rf $RUNTIME_OBJECTS

echo "Removing runtime profiles ..."
printf "  removing files: "
for FILE in machines/*
do
  NAME=$(${YQ} -r .Name $FILE)
  if [[ -f profiles/$NAME.yaml ]]
  then
    printf "."
    rm -f profiles/$NAME.yaml
  fi
  echo ""
done

echo "Removing global profile..."
rm -f profiles/global.yaml

echo "Removing runtime machines and other resources..."
rm -rf machines clusters resource_brokers

echo "Making stub Meta Data file..."
cat > meta.yaml <<EOF
---
Name: NAME
DisplayName: DISPLAYNAME
Author: AUTHOR
CodeSource: CODESOURCE
Color: COLOR
Description: DESCRIPTION
DocUrl: DOCURL
Icon: ICON
License: LICENSE
Prerequisites: PREREQUISITES
Source: SOURCE
Documentation: |
  DOCUMENTATION_HERE
EOF

echo "Clean up unnecessaary object fields..."
printf "  "
for FILE in $(find . -type f | grep -v "^\./templates/")
do
  printf "."
  ${YQ} --inplace "$FILTER" $FILE
done
echo ""

cd ..

echo "Removing intermediate 'backing-store.yaml' content..."
rm -f backing-store.yaml

echo ""
echo "Directory './rw-content' contains cleaned up objects ... "
echo "Example 'bundle' and 'upload' AFTER modifying 'meta.yaml.:"
echo ""
echo "  cd rw-content"
echo "  drpcli contents bundle ../my-content.yaml Version=v1.0.0"
echo "  drpcli contents upload --replace-writable --key=../secrets-key.txt ../my-content.yaml"

echo ""
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "WARNING:  Update 'meta.yaml' with real/correct values."
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""
