package models

import (
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"math/big"
	"testing"
	"time"
)

func rootCertTemplate(now time.Time, expireIn time.Duration) func() (*x509.Certificate, error) {
	return func() (*x509.Certificate, error) {
		serialNumber, err := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
		if err != nil {
			return nil, err
		}
		template := &x509.Certificate{
			SerialNumber: serialNumber,
			NotBefore:    now.AddDate(0, 0, -1),
			NotAfter:     now.Add(expireIn),
			KeyUsage:     x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
			ExtKeyUsage:  []x509.ExtKeyUsage{},
			Subject: pkix.Name{
				Organization: []string{"dr-provision consensus trust root"},
			},
			IsCA:                  true,
			BasicConstraintsValid: true,
		}
		return template, nil
	}
}

func TestHACertRotation(t *testing.T) {
	m := &GlobalHaState{}
	for i := 0; i < 10; i++ {
		m.RotateRoot(rootCertTemplate(time.Now(), 11*time.Second))
		time.Sleep(time.Second)
	}
	if len(m.Roots) != 10 {
		t.Errorf("Expected 10 root certs, not %d", len(m.Roots))
	}
	time.Sleep(12 * time.Second)
	m.RotateRoot(rootCertTemplate(time.Now(), time.Second))
	if len(m.Roots) != 1 {
		t.Errorf("Expected 1 root cert, not %d", len(m.Roots))
	}
}
