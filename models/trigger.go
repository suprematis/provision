package models

import "net/url"

// Trigger defines a server-side work_order to apply Blueprints
// to matching Machines.  Machines are collected by the Filter clause.
//
// swagger:model
type Trigger struct {
	Validation
	Access
	Meta
	Owned
	Bundled
	// Name is the key of this particular Trigger.
	// required: true
	Name string `index:",key"`
	// TriggerProvider is the name of the method of this trigger
	TriggerProvider string
	// Filter is a "list"-style filter string to find machines to apply the cron too
	// Filter is already assumed to have WorkOrderMode == true && Runnable == true
	Filter string
	// Blueprint is template to apply
	Blueprint string
	// Description is a one-line description of the parameter.
	Description string
	// Documentation details what the parameter does, what values it can
	// take, what it is used for, etc.
	Documentation string
	// WorkOrderProfiles to apply to this machine in order when looking
	// for a parameter during rendering.
	WorkOrderProfiles []string
	// WorkOrderParams that have been directly set on the Trigger and will be moved to the work order.
	WorkOrderParams map[string]interface{}
	// Enabled is this Trigger enabled
	Enabled bool

	// Params parameters to tweak the TriggerProvider
	Params map[string]interface{}
	// Profiles to tweak the TriggerProvider
	Profiles []string

	// StoreDataInParameter if set tells the triggers data to be stored in the parameter in the Params of the work_order.
	StoreDataInParameter string
	// MergeDataIntoParams if true causes the data from the trigger to be merged into the Params of the work_order.
	MergeDataIntoParams bool

	// QueueMode if true causes work_orders to be created without a machine, but with a filter for delayed operation
	QueueMode bool
	// AllInFilter if true cause a work_order created for all machines in the filter
	AllInFilter bool
	// FilterCount defines the number of machines to apply the work_order to.  Only one work_order per trigger fire.
	FilterCount int
}

func (r *Trigger) GetMeta() Meta {
	return r.Meta
}

func (r *Trigger) SetMeta(d Meta) {
	r.Meta = d
}

// GetDocumentation returns the object's Documentation
func (r *Trigger) GetDocumentation() string {
	return r.Documentation
}

// GetDescription returns the object's Description
func (r *Trigger) GetDescription() string {
	return r.Description
}

func (r *Trigger) Validate() {
	r.AddError(ValidName("Invalid Trigger Name", r.Name))
}

func (r *Trigger) Prefix() string {
	return "triggers"
}

func (r *Trigger) Key() string {
	return r.Name
}

func (r *Trigger) KeyName() string {
	return "Name"
}

func (r *Trigger) Fill() {
	if r.Meta == nil {
		r.Meta = Meta{}
	}
	if r.WorkOrderProfiles == nil {
		r.WorkOrderProfiles = []string{}
	}
	if r.WorkOrderParams == nil {
		r.WorkOrderParams = map[string]interface{}{}
	}
	if r.Profiles == nil {
		r.Profiles = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}
	if r.FilterCount == 0 {
		r.FilterCount = 1
	}
	r.Validation.fill(r)
}

func (r *Trigger) AuthKey() string {
	return r.Key()
}

func (r *Trigger) SliceOf() interface{} {
	s := []*Trigger{}
	return &s
}

func (r *Trigger) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Trigger)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (r *Trigger) CanHaveActions() bool {
	return true
}

// match Profiler interface

// GetProfiles gets the profiles on this stage
func (r *Trigger) GetProfiles() []string {
	return r.Profiles
}

// SetProfiles sets the profiles on this stage
func (r *Trigger) SetProfiles(p []string) {
	r.Profiles = p
}

// match Paramer interface

// GetParams gets the parameters on this stage
func (r *Trigger) GetParams() map[string]interface{} {
	return copyMap(r.Params)
}

// SetParams sets the parameters on this stage
func (r *Trigger) SetParams(p map[string]interface{}) {
	r.Params = copyMap(p)
}

// SetName sets the name of the object
func (r *Trigger) SetName(n string) {
	r.Name = n
}

// TriggerInput defines the input data for a plugin action that is a trigger
type TriggerInput struct {
	Method   string
	Header   map[string][]string
	Url      *url.URL
	Data     []byte
	Triggers []*Trigger
}

// TriggerResult defines the result to send back to the trigger caller
// Events should be sent if the trigger fires
type TriggerResult struct {
	Data     []byte
	Code     int
	Header   map[string][]string
	HasCount bool
	Count    int
}
