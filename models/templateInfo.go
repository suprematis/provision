package models

import (
	"text/template"

	"github.com/Masterminds/sprig/v3"
)

func DrpSafeFuncMap() template.FuncMap {
	gfm := sprig.GenericFuncMap()

	// Dangerous
	delete(gfm, "env")
	delete(gfm, "expandenv")

	// Misleading
	delete(gfm, "ago")
	delete(gfm, "now")

	return template.FuncMap(gfm)
}

// TemplateInfo holds information on the templates in the boot
// environment that will be expanded into files.
//
// swagger:model
type TemplateInfo struct {
	// Name of the template
	//
	// required: true
	Name string
	// A text/template that specifies how to create
	// the final path the template should be
	// written to.
	//
	// required: true
	Path string
	// Link optionally references another file to put at
	// the path location.
	Link string
	// ID of the template that should be expanded.  Either
	// this or Contents should be set
	//
	// required: false
	ID string
	// Contents that should be used when this template needs
	// to be expanded.  Either this or ID should be set.
	//
	// required: false
	Contents string
	// Meta for the TemplateInfo.  This can be used by the job running
	// system and the bootenvs to handle OS, arch, and firmware differences.
	//
	// required: false
	Meta     map[string]string
	PathTmpl *template.Template `json:"-"`
	LinkTmpl *template.Template `json:"-"`
	// StartDelimiter is an optional start delimiter.
	//
	// required: false
	StartDelimiter string `json:",omitempty"`
	// EndDelimiter is an optional end delimiter.
	//
	// required: false
	EndDelimiter string `json:",omitempty"`
}

func (ti *TemplateInfo) Id() string {
	if ti.ID == "" {
		return ti.Name
	}
	return ti.ID
}

func (ti *TemplateInfo) SanityCheck(idx int, e ErrorAdder, missingPathOK bool) {
	if ti.Name == "" {
		e.Errorf("Template[%d] is missing a Name", idx)
	}
	if !missingPathOK {
		if ti.Path == "" {
			e.Errorf("Template[%d] is missing a Path", idx)
		} else if _, err := template.New(ti.Name).Funcs(DrpSafeFuncMap()).Delims(ti.StartDelimiter, ti.EndDelimiter).Parse(ti.Path); err != nil {
			e.Errorf("Template[%d] Path is not a valid text/template: %v", idx, err)
		}
	}
	if ti.Contents == "" && ti.ID == "" && ti.Link == "" {
		e.Errorf("Template[%d] must have either an ID, a Link, or Contents set", idx)
	}
	if ti.Contents != "" && ti.ID != "" {
		e.Errorf("Template[%d] has both an ID and Contents", idx)
	}
	if ti.Meta == nil {
		ti.Meta = map[string]string{}
	}
}

func (ti *TemplateInfo) PathTemplate() *template.Template {
	return ti.PathTmpl
}

func (ti *TemplateInfo) LinkTemplate() *template.Template {
	return ti.LinkTmpl
}
