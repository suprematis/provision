module gitlab.com/rackn/provision/v4

go 1.19

require (
	github.com/Masterminds/semver v1.4.2
	github.com/Masterminds/sprig/v3 v3.1.0
	github.com/VictorLowther/jsonpatch2 v1.0.0
	github.com/elithrar/simple-scrypt v1.3.0
	github.com/fatih/color v1.7.0
	github.com/fsnotify/fsnotify v1.6.0
	github.com/ghodss/yaml v1.0.0
	github.com/gofunky/semver v3.5.2+incompatible
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.2
	github.com/groob/plist v0.0.0-20190114192801-a99fbe489d03
	github.com/hokaccha/go-prettyjson v0.0.0-20210113012101-fb4e108d2519
	github.com/invopop/jsonschema v0.6.0
	github.com/itchyny/gojq v0.12.3
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869
	github.com/json-iterator/go v1.1.12
	github.com/klauspost/compress v1.16.3
	github.com/klauspost/pgzip v1.2.5
	github.com/krolaw/dhcp4 v0.0.0-20190531080455-7b64900047ae
	github.com/mattn/go-isatty v0.0.12
	github.com/mholt/archiver/v3 v3.5.1
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
	github.com/olekukonko/tablewriter v0.0.4
	github.com/pborman/uuid v1.2.0
	github.com/pkg/xattr v0.4.1
	github.com/shirou/gopsutil/v3 v3.22.5
	github.com/spf13/cobra v1.4.0
	github.com/xeipuuv/gojsonschema v1.1.0
	gitlab.com/rackn/gohai v0.7.7
	gitlab.com/rackn/logger v1.1.1
	gitlab.com/rackn/netwrangler v0.8.1
	gitlab.com/rackn/rofs v0.3.7
	gitlab.com/rackn/seekable-zstd v0.8.1
	gitlab.com/rackn/service v1.1.2
	gitlab.com/rackn/tftp/v3 v3.1.2
	golang.org/x/crypto v0.6.0
	golang.org/x/net v0.7.0
	golang.org/x/sync v0.1.0
	golang.org/x/sys v0.5.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver/v3 v3.1.0 // indirect
	github.com/VictorLowther/godmi v0.6.1 // indirect
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/digitalocean/go-smbios v0.0.0-20180907143718-390a4f403a8e // indirect
	github.com/dsnet/compress v0.0.2-0.20210315054119-f66993602bf5 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/hashicorp/go-hclog v0.16.2 // indirect
	github.com/huandu/xstrings v1.3.1 // indirect
	github.com/iancoleman/orderedmap v0.0.0-20190318233801-ac98e3ecb4b0 // indirect
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/itchyny/go-flags v1.5.0 // indirect
	github.com/itchyny/timefmt-go v0.1.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20211012122336-39d0f177ccd0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.4.0 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	gitlab.com/rackn/simplecache v0.0.0-20230324193231-44368de53d93 // indirect
	golang.org/x/exp v0.0.0-20230213192124-5e25df0256eb // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
