.. Copyright (c) 2021 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Platform documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Platform; SAML-based Single Sign-On

.. _rs_saml_ops:

SAML-based Single Sign-On
=========================

This section will describe how to enable and use SAML single sign-on.

API Access for SAML
-------------------

In dr-provision versions 4.8.0 and later, the DRP server provides two endpoints for
enabling SAML Single Sign-On.

* /login - Provides redirect endpoint for SAML workflow
* /saml/list - Provides a list of possible identity providers

Login
~~~~~

By pointing a browser at `https://drp-ip:api-port/login`, the server will redirect
the user to the identity provider for authentication.  Once this is complete, the login
page will return a token and info structure by default.

Additionally, the `login` endpoint uses two query parameters to provide additional customizations.

* SAML-IDP=<IDP> - The IDP is the name of one of the configured Identity Providers.
* redirect=<Base64 URL> - The redirect option takes a base64 encoded URL that should be redirected to upon success.  A DRP token is appended to the URL as a query parameter.

List
~~~~

By pointing a browser at `https://drp-ip:api-port/saml/list`, a list of SAML Identity Providers is returned.  This
is a smaller list from the Identity Provider objects.  The object contains:

* Name - the name of the IDP to use in the SAML-IDP query parameter
* DisplayName - A display name to use for presenting to the user.
* LogoPath - A path or URL to a logo image.  If the PATH is a full URL, this will be an image.  If the PATH is relative, the file is served from DRP files directory.

These fields map directly to the Identity Provider fields.

UX Access by SAML
-----------------

The UX login page presents the standard authentication page.  If Identity Providers have been configured and functional,
the UX login page will have additional buttons for each of IDP.  The UX will redirect the user to the SAML page for
authentication.  Upon completion, the UX is redirected to the UX system with a DRP token for normal operations.

.. _rs_saml_idp_config:

Identity Provider Configuration
-------------------------------

To enable SAML-based Single Sign-On, the system must have ONE or more Identity Provider configured.  This configuration
requires configuring the IDP with the DRP server's metadata and vice versa.

The SAML Single Sign-On uses the API server's certificate and key for identity.  If this certificate/key pair is changed,
the metadata at the IDP must be updated.

Register DRP with IDP
~~~~~~~~~~~~~~~~~~~~~

The DRP server must be registered with the IDP.  The identity of the DRP server is first configured value in order:

* Server Host Name
* HA Id
* DRP Id
* Server Virtual IP
* Server Static IP

With the identity established, registration be done one of two ways.

For some IDPs, they can be given a URL to get the metadata automatically.  The metadata URL is `https://drp-ip:api-port/saml/metadata`.
Entering this URL into the IDP will cause the IDP to query the DRP Server for the required certificate and identity information.

The other is to save the metadata into a file.  This file will be XML data that can be upload to your identity provider.

  ::

    curl -k https://drp-ip:api-port/saml/metadata -o metadata.xml

This metadata.xml file can be upload to your identity provider.  The file can be reused for all Identity Providers.



Register IDP with DRP
~~~~~~~~~~~~~~~~~~~~~

This requires creating an Identity Provider in the DRP seystem.  This can be either done through the UX or the CLI.

The required fields for the IDP are:

* Name - this is the short name of the IDP.
* DisplayName - this is the displayed name for us in the UX
* LogoPath - this is an optional URL or relative file path into the DRP server.  It presents an image for display.
* MetaDataUrl - This the URL to the IDP's metadata.
* MetaDataBlob - This is the XML file from the IDP stored as a string on the object.

Upon loading into DRP or DRP startup, the Metadata is validated directly or URL is queried to get the data and validated.

Once both steps are done, SAML can be used for authentication.

Additional Customization
~~~~~~~~~~~~~~~~~~~~~~~~

Each Identity Provider in DRP will also define the access control path for the authenticated users.

There are two attributes that are delivered by the SAML Assertions.  The first attribute is used to define the
username.  This is the `UserAttribute` field.  This maps the SAML IDP identity into the Name of a User.  This is usually
an email for ease of use, but is not required.

The second attribute is used to define groups that should apply to the user.  This is the `GroupAttribute` field.  The
attribute in the SAML assertion should be a comma separated list of groups that the user is a member of.

The Identity Provider has three additional fields that control what access the user should have.

* DenyIfNoGroups - If no groups are present or none match, access is denied that user.
* DefaultRole - This is the default role that should be given to the User if no groups match.  The DenyIfNoGroups overrides this.
* GroupToRole - This is a map of group names to lists of roles.   It is a key value pair, where the key is the group name and the value is a list of roles to apply.

.. _rs_saml_idp_auth:

Identity Provider Examples
--------------------------

AWS
~~~

An example yaml file for AWS:

  ::

    ---
    DefaultRole: "superuser"
    DenyIfNoGroups: false
    DisplayName: AWS IAM
    GroupAttribute: "groups"
    GroupToRoles:
      role1:
        - operator
      role2:
        - superuser
    MetaDataUrl: https://portal.sso.us-west-2.amazonaws.com/saml/metadata/Nzg0NDI0NjE0NTk2X2lucy03MjJmMGU0NjQ5OTRiNzNk
    Name: aws-west-2
    UserAttribute: "username"


Google
~~~~~~

For Google, you will need to use go to your Google Workspace Admin page.  Navigate to App -> Web and mobile apps.

You will need to "Add app -> Add custom SAML app".   You will need to specify an app name, this can be anything along with a description.

At this point, you can download metadata.  The contents of this file will need to be added to your IDP object as a MetadataBlob.

With that set, the next pieces to fill in are the ACS URL and the Entity ID.  This can be found by selecting Download Metadata in the
DRP UX. This file contains the EntityID.  This is the value can be found here or the order is defined above.  The ACS URL
is specified near the end of the line.  It looks like this:

  ::

    <AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="https://webhook.rackn.io:8092/saml/acs" index="1"></AssertionConsumerService>

The URL is usually https://<server name or ip>:<api port>/saml/acs.  If the ServerHostname is specified, it will be used.  Otherwise it will be
the IP address of the DRP endpoint.

Specify the Name ID format as email.  This must be specified, but isn't really used.  The registered attributes in the next section as
used for group matching.

Add Mapping attributes for the username.  Click Add Mapping, select primary email and name the attribute email.
Add Mapping attributes for group memberships.  Select the groups you want to map to the groups attribute in the bottom section.

Click Finish.

In DRP, create an IDP object for Google.  It would look like this:

  ::

    DefaultRole: ''
    DenyIfNoGroups: true
    DisplayName: RackN Google Auth
    GroupAttribute: groups
    GroupToRoles:
      Engineering:
        - superuser
    LogoPath: https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/240px-Google_%22G%22_Logo.svg.png
    MetaDataBlob: blob from google
    Name: GoogleAuth
    UserAttribute: email

Note that UserAttribute matches the attribute containing the email of the user.  Note that GroupAttribute matches the attributing containing
groups list.

There are some issues with this integration.  You will have to clear current google token/auth cookies to get new enabled cookies from Google.
Additionaly, if you have multiple google accounts, you may have to log out of the accounts that you don't want to use to login.  Using
an incognito window can help this.
