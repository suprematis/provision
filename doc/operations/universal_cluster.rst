.. Copyright (c) 2021 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Platform documentation under Digital Rebar master license
..
.. doc hierarchy: ~ - =
.. index::
  pair: Digital Rebar Platform; Universal Workflow Cluster

.. _rs_universal_cluster:

Universal Cluster Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section will address usage of the Universal Workflow system for Clusters (v4.8+).  The architecture and implementation of the Universal
Workflow system is described at :ref:`rs_universal_arch`.

Note: the cluster feature is specific to v4.8+

.. _rs_universal_cluster_intro:

Clusters
--------

Clusters are used to coordinate operations over groups of machines.  All machines in a cluster
will reference the cluster's tracking profile using the `cluster/tags` param instead of the legacy
`cluster/profile` params.  Since clusters have their own workflow and lifecycle; they are able
act outside of their machines' lifecycle.  This includes building, resizing or destroy clusters, 
managing platforms from their platform API, and other non-machine actions.

.. _rs_universal_cluster_create:

Creating a Cluster
==================

To create a cluster, the `universal-cluster-provision` workflow must be included.

The workflow chain map of `cluster` should be used and will execute from `universal-start` to include the 
cluster create workflow.

.. _rs_universal_cluster_destroy:

Destroying a Cluster
====================

To destroy a cluster, the `universal-cluster-destroy` workflow must be included.

The workflow chain map of `cluster` should be used and will execute from `universal-destroy` to include the 
cluster destroy workflow.
