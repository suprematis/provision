.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Release v4.11
  pair: Digital Rebar Provision; Release Notes

.. _rs_release_v4_11:

Digital Rebar version 4.11 [January 2023]
----------------------------------------

Release Date: January 12, 2023

Release Themes: Tuning and Performance

Executive Summary: In addition to bug fixes and performance improvements, the release includes many customer-driven features that improve the usability and scale of the platform for operators.

See :ref:`rs_release_summaries` for a complete list of all releases.

.. _rs_release_v4_11_notices:

Important Notices
~~~~~~~~~~~~~~~~~

New for this release:

* v4.11 Discovery image now defaults to Alma Linux.  Users upgrading Digital Rebar should also install and migrate to this new Sledgehammer.
* v4.11 Client certificate authentication is now off by default in v4.10 and v4.11.  To enable, add the `--client-cert-auth`` flag or the `RS_CLIENT_CERT_AUTH=true`` flag to the systemd configuration space.  install.sh has been updated for these options.

From prior releases:

* v4.10 Debian-8 has been removed from the content pack
* v4.10 To enable/disable read only Triggers, add the parameter `trigger/<name of trigger>-[enabled|disabled]: true` in the global profile. See :ref:`rs_workorder_trigger`.
* v4.10 Fixed critical bug in event registration that caused system crash if request is malformed.  This is a recommended dot upgrade for previous releases and was back ported to v4.6 and later releases.
* v4.10 Interactive labs can be used to explore and learn the latest Digital Rebar features
* v4.10 EventToAudit plugin allows any event to be mapped to an audit log item
* v4.9 Digital Rebar cannot run in a container when using features including Contexts, Brokers, Clusters, Multi-Site Manager and High Availability.  These features all rely on Digital Rebar managing both it's own lifecycle and driving containers.  Running in a container prevents Digital Rebar from performing operations required for these features.
* v4.9 work_order_templates (preview in v4.8) have been renamed to be blueprints
* v4.9 removal of the Centos mirrors require users to use an archival Centos8 mirror for Sledgehammer8.
* v4.9 format for content pack meta data changed, you must use the v4.9 drpcli to bundle v4.9 content
* v4.8 Workflows relating to Clusters and Cloud_Wrappers have been significantly changed.  Operators using those patterns should carefully review their implementation before upgrading.
* v4.8 Cleanup operation offers safer option than destroy by enabling pre-destroy actions by defining `on-delete-workflow`.  Operators should migrate destroy to cleanup where possible.
* v4.8 Portal UX editing panels on several screens have been signficantly changed.  UXViews ability to limit machine field view/edit is no longer enabled in v4.8.
* v4.8 Context Containers need to be rebuilt for v4.8+ Docker Context so as to NOT inject a DRPCLI copy.  To ensure that the DRPCLI version matches the DRP endpoint, the Docker Context will now automatically attach the correct DRPCLI to containers during the starting process. See :ref:`rs_release_v48_contexts`.
* v4.8 added Workflows relating to Clusters and Cloud_Wrappers have been significantly changed.  Operators using those patterns should carefully review their implementation before upgrading.
* v4.7 added port 8090 to the list of ports _required_ for provisioning operations. Please verify that port 8090 (default, this can be changed) is accessible for Digital Rebar endpoints.
* v4.7 changed the install zip format, the API-based upgrade of DRP to v4.7+ requires usage of most recent https://portal.RackN.io (v4.7 for self-hosted UX users) or the use of DRPCLI v4.6.7+. The v4.7 ``install.sh upgrade`` process also includes theses changes.
* v4.6 changed mananging of signed certificates, the process for updating and importing certificates has changed to require using the DRPCLI or API to update certificates.  See :ref:`rs_cert_ops` for details.

.. _rs_release_v4_11_vulns:

Vulnerabilities
+++++++++++++++

OpenSSL vulnerabilities fixed by OpenSSL v3.0.8 (official CVE ID pending) have been addressed in Digital Rebar v4.11.  This includes updates to all discovery images and Golang packages (see Golang Vulnerability Checking below).

The following vulnerabilities were reported during the release cycle and corrected before the release; consequently, they are not concerns for v4.11 users.

:ref:`_rs_cve_2022_46382`
:ref:`_rs_cve_2022_46383`

.. _rs_release_v4_11_deprecations:

Deprecations
++++++++++++

None known

.. _rs_release_v4_11_removals:

Removals
++++++++

None known

FEATURES
~~~~~~~~

Is Machine Alive? Status (Connections API)
++++++++++++++++++++++++++++++++++++++++++

Beyond the Machine's `Runnable` property, Digital Rebar will now use the runner's active connection to determine if a managed machine is online or not.  Since this data is DROCLI connection related and not machine specific, the information is exposed via the `connections` API and not part of object properties.

This allows operators to better track the status of infrastructure by providing direct and ongoing knowledge of a machine's availility using the runner as a proxy.

The implementation of this feature includes a DRPCLI connections command that allows operators to inspect the status of all connected systems.

Since this feature relies on the runner, it does not track Alive status for non-managed machines.

A side benefit of this feature is that Digital Rebar is now able to track DRPCLI connections also.

UX Views Plugin integrated into Digital Rebar platform
++++++++++++++++++++++++++++++++++++++++++++++++++++++

Digital Rebar UX has provided UX configuration such as custom menus, filters and lists for many years using the UX plugin.  In v4.11, the UX plugin data tables were incorporated into the core platform so the plugin is nolonger required.  The UX Views plugin was a data only plugin that used to enable advannced UX behaviors such as specialized behaviors, views and filters.  To make these features more accessible, the objects are being incorporated into the base Object model.

No migration actions are required.  In v4.11, the system will automatically disable the UX plugin if it was installed and uses the same schema for data storage.

Live Machine Migrate
++++++++++++++++++++

Operators can now transfer live machines between DRP endpoints including endpoints of different versions.  This allows operators to migrate existing machines from a running Digital Rebar server to a new Digital Rebar server.  This allows operators to reduce operational risk associated with upgrading Digital Rebar management tooling.

This is an advanced operational procedure, please contact RackN for assistance before attempting.

Additional Discovery Images (Sledgehammer)
++++++++++++++++++++++++++++++++++++++++++

As part of an ongoing migration away from Centos dependencies, RackN has introduced several new options for discovery images.

Centos discovery remains available for customers who have requirements for the legacy Sledgehammer.

* Alma Based Discovery (new detault): The Alma-based Sledgehammer replaces Centos with minimial impact for discovery processes and will be the default discovery environment for v4.11 and later releases.
* Ubuntu Discovery ("Ubuntu Hammer"): The Ubuntu discovery image is based on Ubuntu (aka "Ubuntu Hammer") Allows operators to use Ubuntu 22.04 and select Debian packaged tools and utilities: specifically on enabling Curtin.  This version of Sledgehammer not intented for general use-caes, is designed specifically for image-based deployments using Curtin.

VMware Cluster Building (CloudBuilder automation)
+++++++++++++++++++++++++++++++++++++++++++++++++

Coordinate cluster building activities for completed vCenter build out including VSAN, NSX-T using vmware lib and other tools.

Using Digital Rebar cluster life-cycles and pipelines, Digital Rebar is able to automate the CloudBuilder process from bootstrapping the OVA to driving the CloudBuilder API through a complete install sequence.  The process also modifies the CloudBuilder JSON to inject machine specific details into the process as needed.

Additonal details on https://rackn.com/2022/08/25/fully-automated-vmware-cloud-builder-and-cloud-foundations/

Proxmox VM create via CloudWrapper
++++++++++++++++++++++++++++++++++

Added a Proxmox Terraform broker that can be used to provision clusters of machines in Proxmox platforms.  Unlike cloud brokers, this process relies on PXE boot intead of known image deploy.

For operators using Proxmox, this allows full life-cycle automation of the VM create, update and destroy processes.  It complements Digital Rebar Proxmox cluster building automation by allows operators to both deploy the platform and systems on the platform.

Proxmox is an open source alternative to VMware and other private cloud platforms.

Pipeline: Universal Hardware components are replacable by Profile
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Dgitial Rebar v4.11 will be more accomodating of hardware variances without RackN engineering.

Speciic vendor hardware implmentations are now managed by the shared universal hardware workflow without having to use the same code paths.  Operators can set profiles to define exactly which hardware toolchain components are used during the Hardwawre stage of the universal pipeline.

This eliminates the needs to have a shared / least common denominator system based on the RackN hardware specific plugins.  With v4.11 customers and vendors can write and manage their own hardware configuration tools and processes.

Ansible Apply on Cluster builds Inventory
+++++++++++++++++++++++++++++++++++++++++

When using the `Ansible Apply` task on a Cluster, the task automatically generates a dynamic Inventory based on the Machines attached to the cluster and runs Ansible from the Digital Rebar endpoint.  For single machine Ansible, the same playbooks will be run locally.

This gives operators the flexiblity to easily use the same Ansible in remote (ssh) or local modes without modification.

Tech Preview: ISOfs (ISO file system) 
++++++++++++++++++++++++++++++++++++++

ISOfs provides file system mapping for large files that have previously been decomposed into smaller elements.  This eliminates the need to expand ISOs and Tarballs; instead, files in these artifacts are available from the sources.

By eliminating the need to expand ISOs, Digital Rebar v4.11 can significantly reduce the storage space allocated.  It also eliminates the overhead incurred by the system when uploading ISOs for provisioning.

This feature is available via configuration in v4.11.  Customers who believe they would benefit from ISOfs should contact RackN for help activating the feature.

.. _rs_release_v4_11_otheritems:

General UX Improvements (applies to all DRP versions)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Promethus Metrics View
* Fix template adding having empty path
* Fix upgrade button banlist not disabling upgrades
* Fix slim sent when specific params requested
* Improve UX upgrade processes
* UX Event button trigger presents a user form based parameters listed in `Meta.required` 

Other Items of Note
~~~~~~~~~~~~~~~~~~~

* Enhanced IaC catalog building process
* Improvements and fixes to HPE firmware support
* Make Linode join-up StackScript more reslient (repond to Linode image changes)
* Update AWS Terraform to be explicit about default VPC (respond to AWS API changes)
* Install RS_SERVER_HOSTNAME explicitly sets the DRP endpoint hostname if needed
* Fix gohai for reading of disk serial numbers (community contribution by Arthur Outhenin-Chalandre)
* Incorporate Golang Vulncheck in automated builds
* IPMI performance improvements by improving use of onecli calls
* Context for NAPALM tools available for switch automation
* Refactor of IPMI quirks (helps track differences between system out of band management APIs)
* Address boot loader variations in Ubuntu 20.04+

Roadmap Items (unprioritized, planned for future release)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Updated to Hardware Vendor tool chains - updates RAID/BIOS/Firmware to use updated tools from OEMs
* Agentless ESXi Install - replace/rewrite of the existing ESXi agent (drpy) based on upgraded VMware and customer requirements
* Integrated Simple DNS - provide DNS services for networks
* Integrated Billing - integrate Billing plugin functionality into Digital Rebar core
* IPv6 DHCP - provide DHCP services for IPv6 networks
* Restricted Access ISOs - allow authenticated users to download non-public ISOs managed by RackN
