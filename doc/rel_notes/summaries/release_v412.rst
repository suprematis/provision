.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Release v4.12
  pair: Digital Rebar Provision; Release Notes

.. _rs_release_v4_12:

Digital Rebar version 4.12 [in process]
---------------------------------------

Release Date: Q2 2023

Release Themes: Platform Team Enablement

TBD Executive Summary

See :ref:`rs_release_summaries` for a complete list of all releases.

.. _rs_release_v4_12_notices:

Important Notices
~~~~~~~~~~~~~~~~~

New for this release:

* TBD

From prior releases:

* copy from prior

.. _rs_release_v4_12_vulns:

Vulnerabilities
+++++++++++++++

None known

.. _rs_release_v4_12_deprecations:

Deprecations
++++++++++++

None known

.. _rs_release_v4_12_removals:

Removals
++++++++

None known

FEATURES
~~~~~~~~

**No committed features at this time**

Work in Process Items

These items are under active development, but cannot be confirmed for delivery in this release.  Please consult with RackN if items below are mission critical for your operation.

EKS Cluster Automation
++++++++++++++++++++++

TBD

"Just run my Plan" Terraform and Ansible Runners
++++++++++++++++++++++++++++++++++++++++++++++++

Streamlines existing Ansible and Terraform processes to allow users to run plans without first modifying them to comply with Digital Rebar cluster requirements.

This allows operators to quickly import working Terraform plans to take advantage of Digital Rebar TF state store, orchestration and tracking without having to decompose the plan into Digital Rebar templates.

This streamlines initial use cases and provides a path for operators to leverage Digital Rebar broker and cluster abstractions.

.. _rs_release_v4_12_otheritems:

General UX Improvements (applies to all DRP versions)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* TBD

Other Items of Note
~~~~~~~~~~~~~~~~~~~

* TBD

Powershell join-up
++++++++++++++++++

Create a Windows version of the Digital Rebar join-up script in addition to the existing Linux and ESXi version.  This allows operators to add existing Windows systems into Digital Rebar without having to go through a Linux-based discovery.

Out of Band Machine Audit capabilities
++++++++++++++++++++++++++++++++++++++

Using Digital Rebar Orchestration and Work Order capabilities, operators will be able to perform comprehensive reviews of machine assets.  This enables Enterprise to perform a comprehensive audit of machines even if they were not previously managed by Digital Rebar.

This includes:

  * Leveraging Worker sets
  * Enabling Batch Environments and Batch Blueprints
  * Hardware Redfish Audit Blueprint
  * Audit Views / Reports

Roadmap Items (unprioritized, planned for future release)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Agentless ESXi Install - replace/rewrite of the existing ESXi agent (drpy) based on upgraded VMware and customer requirements
* Integrated Simple DNS - provide DNS services for networks
* Integrated Billing - integrate Billing plugin functionality into Digital Rebar core
* IPv6 DHCP - provide DHCP services for IPv6 networks
* Restricted Access ISOs - allow authenticated users to download non-public ISOs managed by RackN
