.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Release v4.9
  pair: Digital Rebar Provision; Release Notes

.. _rs_release_v49:

Digital Rebar version 4.9 [March 2022]
-----------------------------------------

Release Date: March 1, 2022

Release Themes: Resource Management Abstractions

In addition to bug fixes and performance improvements, the release includes several customer-driven features.

See :ref:`rs_release_summaries` for a complete list of all releases.

.. _rs_release_v49_notices:

Important Notices
~~~~~~~~~~~~~~~~~

New for this release:

* v4.9 Digital Rebar cannot run in a container when using features including Contexts, Brokers, Clusters, Multi-Site Manager and High Availability.  These features all rely on Digital Rebar managing both it's own lifecycle and driving containers.  Running in a container prevents Digital Rebar from performing operations required for these features.
* v4.9 work_order_templates (preview in v4.8) have been renamed to be blueprints
* v4.9 removal of the Centos mirrors require users to use an archival Centos8 mirror for Sledgehammer8.
* v4.9 format for content pack meta data changed, you must use the v4.9 drpcli to bundle v4.9 content

From prior releases:

* v4.8 Workflows relating to Clusters and Cloud_Wrappers have been significantly changed.  Operators using those patterns should carefully review their implementation before upgrading.
* v4.8 Cleanup operation offers safer option than destroy by enabling pre-destroy actions by defining `on-delete-workflow`.  Operators should migrate destroy to cleanup where possible.
* v4.8 Portal UX editing panels on several screens have been signficantly changed.  UXViews ability to limit machine field view/edit is no longer enabled in v4.8.
* v4.8 Context Containers need to be rebuilt for v4.8+ Docker Context so as to NOT inject a DRPCLI copy.  To ensure that the DRPCLI version matches the DRP endpoint, the Docker Context will now automatically attach the correct DRPCLI to containers during the starting process. See :ref:`rs_release_v48_contexts`.
* v4.8 added Workflows relating to Clusters and Cloud_Wrappers have been significantly changed.  Operators using those patterns should carefully review their implementation before upgrading.
* v4.7 added port 8090 to the list of ports _required_ for provisioning operations. Please verify that port 8090 (default, this can be changed) is accessible for Digital Rebar endpoints.
* v4.7 changed the install zip format, the API-based upgrade of DRP to v4.7+ requires usage of most recent https://portal.RackN.io (v4.7 for self-hosted UX users) or the use of DRPCLI v4.6.7+. The v4.7 ``install.sh upgrade`` process also includes theses changes.
* v4.6 changed mananging of signed certificates, the process for updating and importing certificates has changed to require using the DRPCLI or API to update certificates.  See :ref:`rs_cert_ops` for details.

.. _rs_release_v49_vulns:

Vulnerabilities
+++++++++++++++

The following vulnerabilities were reported

* :ref:`_rs_cve_2022_46382`
* :ref:`_rs_cve_2022_46383`

.. _rs_release_v49_deprecations:

Deprecations
++++++++++++

.. _rs_release_v49_removals:

None known

Removals
++++++++

* Some Work Order objects have been renamed from the v4.8 preview, `work_order_templates` are `blueprints` and `repeating_work_orders` are `work_order_triggers` in this and future releases.
* pre-Universal Bootstrap (bootstrap-base, bootstrap-advanced, bootstrap-contexts) workflows and related stages will be removed in v4.9.  This functionality has been migrated to bootstrap-universal.
* pre-v48 k3s does not comply with new cluster models and has been removed (needs to be migrated for v4.8+ clusters)
* Reposted from v4.8:

  * The single machine behavior of Cloud_Wrapper has been removed.  The v4.8 Cloud_Wrapper works strictly with the Resource Broker model.
  * The capability for Machine Panel field restriction for UX Views has been removed in v4.8.  If you are using this capability, please let RackN know.
  * The pre v4.6 cluster patterns have been removed.  The v4.8 Cluster utilities work strictly with the Cluster model.


FEATURES
~~~~~~~~

.. _rs_release_v49_new_objects:

UX: Pages for Resource Brokers, Clusers and Work Orders
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

To support v4.8+, the v4.9 UX added several dedicated sections.

The first was "Resources" which included the new cluster and resource broker types.
The UX included dedicated tables and pages for Clusters and Resource Brokers.
The Resource group includes moving the Machines and Pools into a new Resources group.

The second was "Services" which included the new work_orders, blueprints and triggers types.
The UX included dedicated tables and pages for Work Orders, Triggers, Blueprints and Tigger Providers.
The Service group include moving the Jobs into the new Services group.

.. _rs_release_v49_pipeline:

UX: for Infrastructure Pipelines / Universal Workflow
+++++++++++++++++++++++++++++++++++++++++++++++++++++

The UX offers Pipelines (Universal Workflow) components as the primary configuration
process.  Pipelines are a specialized Profiles that Digital Rebar uses to provide
more advanced Workflow and configuration options.  For brevity, Pipeline selectors
generally remove the "universal-applications" prefix from the Pipeline profiles.

Legacy Workflows are still available in the control processes, however, pages are
designed with Pipeline usability as the priority.  This will offer options which
do not apply and should be ignored for pre-Universal Applications customers.

.. _rs_release_v49_ux_form:

UX: Field by Field Saving
+++++++++++++++++++++++++

Changes made to UX forms are now saved automatically when focus is lost for the edited field
rather than requiring a "save" or "update" action for the form.  Errors or warnings are provided
immediately at at the specific field causing the error.

Like previous form updates, the field-by-field process uses PATCH updates behind the scenes.

.. _rs_release_v49_bulk:

UX: New Batch Edit
++++++++++++++++++

The UX has replaced the quick editing tabs on the Machine page (aka "bulk edit") with a
batch editing feature that allows operators to accumulate several chanages and apply them
as an atomic PATCH operation on selected matchines.

The batch editing feature is accessible via a new "Actions" dropdown menu and provides
additional edit and section capabilities.

.. _rs_release_v49_inventory:

Cloud Inventory and Cost Calculation
++++++++++++++++++++++++++++++++++++

The inventory process now includes a `cost-calculator` stage that will populate the `inventory/cost`
parameter on machines, clusters and resource brokers. At this time, cloud cost estimates
are self-managed via entries in the `cloud/cost-lookup`.  That requires operators to provide
their own cost estimates based on machine instance types per provider.

The UX Overview has been updated to show the cluster rollup costs.  Please note that cluster
costs rely on machine costs being accurately calculated.  The `cost-calculator` blueprint has been
included to perform ad hoc updates for working clusters and brokers.

In the future, this could be integrated with a cloud pricing service like Mist.io or similar.

.. _rs_release_v49_triggers:

Tiggers via Time, Events and Web Hooks
++++++++++++++++++++++++++++++++++++++

Using the new Blueprints and Blueprint Triggers, operators can create a variety of triggers for
Digital Rebar Work Orders.  The triggers can be setup to detect internal events or operate on a
specific time using chron-like notation.

Triggers can also be setup as targets for inbound Webhook calls by 3rd parties assuming that the
Digital Rebar site is accessible and the operator assigned token is provided.  The `triggers` plugin
is required for this feature.


.. _rs_release_v49_airgap:

Air Gap Installation
++++++++++++++++++++

To support customers with environments that cannot connect or download sources from the internet
(aka "air gapped"), the Digital Rebar installation can be packaged to include all needed components.

The process includes the ability to collect all needed components for an air gapped installation.

Both install.sh and drpcli in v4.9 have been updated to enable this feature.

.. _rs_release_v49_sledge8:

Sledgehammer Centos8 Mirror Changed
+++++++++++++++++++++++++++++++++++

The change of the default Cento8 mirror requires that operators update their Sledgehammer mirror
to use an archival mirror (vault).

This fix can be back ported to previous DRP installations updating the package-repositories Param
to use https://vault.centos.org/8.4.2105/BaseOS/ppc64le/os instead of http://mirrors.edge.kernel.org/.

The RackN team is actively working on non-Centos alternatives for Sledgehammer.

Note: this change also impacts cloud users of Cento8 and the defaults for Cloud Images have been changed
where appropriate.

.. _rs_release_v49_proxmox:

ProxMox cluster building
++++++++++++++++++++++++

This release Digital Rebar pipeline that installs ProxMox using an Infrastructure Pipeline and then
uses the cluster and resource broker features to create and manage VMs in the cluster.


.. _rs_release_v49_kubespray:

Kubespray cluster building
++++++++++++++++++++++++++

Kubespray, the Ansible Kubernetes install process, has been migrated to use the v4.8+ Cluster and Resource Broker installation patterns.
Several configurations are supported in this process including single node and HA.

This includes using an Ansible Context based on the community supporting Kubespray Ansible instead of the Digital Rebar Ansible context.

.. _rs_release_v49_multisite_demo:

Multi-Site Demo Cluster
+++++++++++++++++++++++

RackN has migrated the multi-site demostration setup to use v4.8+ cluster processes instead of being
driven from external scripts.  In this new process, the Multi-Site-Cluster-Demo pipeline will automatically
build a multi-site system including setting up and joining DRP endpoints into the manager.  When

While this should work with any broker, RackN exclusively tests this configuration using Linode so your results
on other clouds may vary.

Note: your DRP must be set to manager mode for this to operate.

.. _rs_release_v49_otheritems:

Other Items of Note
~~~~~~~~~~~~~~~~~~~

* Improved loggin in the callback plugin
* Simplify certificate rotation for HA setups
* Changed when license versions are updated so that end point additions do not trigger a global license update notice unless other entitlements have changed.
* Fix UEFI boot ordering
* Machine naming for AWS instances created by the AWS broker fixed to match DRP names.
* Improved support for Amazon Linux
* UX uses meta data for required and optional params in cluster create

