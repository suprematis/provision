.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _how_to_update_mac_addresses_for_machine:
.. _rs_kb_00070:

kb-00070: How to Update MAC Addresses for Machine (eg change NIC)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00070


Description
-----------

It may be necessary from time to time to change the Network Interface Card(s) (NICs) in
systems.  The NIC Media Access Control (MAC) Addresses are used to uniquely identify
machines, and as a consequence, this will cause the DRP Endpoint to not associate the
Machine object and the physical system correctly.

In these cases, it is necessary to change the Machines registered MAC Addresses in the
Machine object.  Subsequent "discovery" processes will not correctly correlate the Machine
to the Machine object via the new MAC Addresses.

Fortunately, it's very easy to update the Machine object with the Machine's newly updated
MAC Addresses.

The MAC Addresses are tracked in the Machine objects ``HardwareAddrs`` JSON top level field.


Solution
--------

To modify the MAC Addresses associated with a given Machine, update the ``HardwareAddrs``
top level field on the Machine.

The Machine UUID or the Machine's "``Name``" field can be used to select which Machine to
update.  In the below examples, we will use a Machine UUID as referenced by the shell
variable ``RS_UUID``.  Please update the ``RS_UUID`` variable correctly, or substitute
the command example directly with the UUID or Name (eg ``Name:fluffy.example.io``).

View the current MAC Addresses:

  ::

    # set "RS_UUID" appropriately
    drpcli machines show $RS_UUID | jq -r '.HardwareAddrs'

Example update command with ``drpcli``:

  ::

    # set "RS_UUID" appropriately
    # set correct MAC Addresses appropriately to your Machine
    drpcli machines update Name:test '{ "HardwareAddrs": [ "12:34:56:00:00:01", "12:34:56:00:00:02" ] }'

The previous "View" command should now show you the updated MAC Addresses in the Machine object.



Additional Information
----------------------

Additional resources and information related to this Knowledge Base article.


See Also
========


Versions
========


Keywords
========


Revision Information
====================
  ::

    KB Article     :  kb-00070
    initial release:  Fri Nov  5 07:24:37 PDT 2021
    updated release:  Fri Nov  5 07:24:37 PDT 2021

