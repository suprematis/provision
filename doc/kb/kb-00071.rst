.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _rs_DHCP_Ignoring_Request_Message:
.. _rs_kb_00071:

kb-00071: DHCP Ignoring Request Message
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00071


Description
-----------
When using an external DHCP server the logs on DRP show a message about ignoring DHCP request.

  ::

    [12:07] xid 0x131b7f62: Ignoring request for DHCP server 10.218.15.1   dhcp dhcp.go:682




Solution
--------
PXE/iPXE booting may or may not be working and you see the following message:

  ::

    [TIME] xid HEXVALUE: Ignoring request for DHCP server <IP>     dhcp dhcp.go:LINENO

This message is going to be logged when our DHCP logging level is set to at least ``Warn``. You can set the
log level to ``Error`` or if you are not using us as a DHCP server at all you can also start DRP with the
``--disable-dhcp`` flag. This message is not an error. It is expected, and is safe to ignore. What it
is saying is that the DHCP server within DRP is ignoring a request, which is what you want when you are not
using it as the DHCP server.

You can change the log level from the UX by going to the Info & Prefs page, and selecting ``Error`` for the DHCP
logging level, and saving your changes.

You can change the log level from the CLI as well by running the following command:

  ::

    drpcli prefs set debugDHCP "error"



Additional Information
----------------------

Additional cli flags may be needed to set the drp endpoint, username, or password.
See :ref:`rs_drpcli` help text (eg `drpcli --help`) for more options.


See Also
========
* :ref:`rs_kb_00010`
* :ref:`rs_drpcli`

Versions
========


Keywords
========
drpcli, dhcp, external dhcp

Revision Information
====================
  ::

    KB Article     :  kb-00071
    initial release:  Mon 22 Aug 2022 09:26:20 AM CDT
    updated release:  Mon 22 Aug 2022 09:26:20 AM CDT

