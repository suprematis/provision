
.. _rs_drpcli_resource_brokers:

drpcli resource_brokers
-----------------------

Access CLI commands relating to resource_brokers

Options
~~~~~~~

::

     -h, --help   help for resource_brokers

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli resource_brokers
   action <drpcli_resource_brokers_action.html>`__ - Display the action
   for this resource_broker
-  `drpcli resource_brokers
   actions <drpcli_resource_brokers_actions.html>`__ - Display actions
   for this resource_broker
-  `drpcli resource_brokers add <drpcli_resource_brokers_add.html>`__ -
   Add the resource_brokers param *key* to *blob*
-  `drpcli resource_brokers
   addprofile <drpcli_resource_brokers_addprofile.html>`__ - Add profile
   to the resource_broker’s profile list
-  `drpcli resource_brokers
   addtask <drpcli_resource_brokers_addtask.html>`__ - Add task to the
   resource_broker’s task list
-  `drpcli resource_brokers
   await <drpcli_resource_brokers_await.html>`__ - Wait for a
   resource_broker’s field to become a value within a number of seconds
-  `drpcli resource_brokers
   bootenv <drpcli_resource_brokers_bootenv.html>`__ - Set the
   resource_broker’s bootenv
-  `drpcli resource_brokers
   cleanup <drpcli_resource_brokers_cleanup.html>`__ - Cleanup
   resource_broker by id
-  `drpcli resource_brokers
   count <drpcli_resource_brokers_count.html>`__ - Count all
   resource_brokers
-  `drpcli resource_brokers
   create <drpcli_resource_brokers_create.html>`__ - Create a new
   resource_broker with the passed-in JSON or string key
-  `drpcli resource_brokers
   currentlog <drpcli_resource_brokers_currentlog.html>`__ - Get the log
   for the most recent job run on the resource_broker
-  `drpcli resource_brokers
   deletejobs <drpcli_resource_brokers_deletejobs.html>`__ - Delete all
   jobs associated with resource_broker
-  `drpcli resource_brokers
   destroy <drpcli_resource_brokers_destroy.html>`__ - Destroy
   resource_broker by id
-  `drpcli resource_brokers etag <drpcli_resource_brokers_etag.html>`__
   - Get the etag for a resource_brokers by id
-  `drpcli resource_brokers
   exists <drpcli_resource_brokers_exists.html>`__ - See if a
   resource_brokers exists by id
-  `drpcli resource_brokers get <drpcli_resource_brokers_get.html>`__ -
   Get a parameter from the resource_broker
-  `drpcli resource_brokers
   group <drpcli_resource_brokers_group.html>`__ - Commands to control
   parameters on the group profile
-  `drpcli resource_brokers
   indexes <drpcli_resource_brokers_indexes.html>`__ - Get indexes for
   resource_brokers
-  `drpcli resource_brokers
   inserttask <drpcli_resource_brokers_inserttask.html>`__ - Insert a
   task at [offset] from resource_broker’s running task
-  `drpcli resource_brokers
   inspect <drpcli_resource_brokers_inspect.html>`__ - Commands to
   inspect tasks and jobs on machines
-  `drpcli resource_brokers jobs <drpcli_resource_brokers_jobs.html>`__
   - Access commands for manipulating the current job
-  `drpcli resource_brokers list <drpcli_resource_brokers_list.html>`__
   - List all resource_brokers
-  `drpcli resource_brokers meta <drpcli_resource_brokers_meta.html>`__
   - Gets metadata for the resource_broker
-  `drpcli resource_brokers
   params <drpcli_resource_brokers_params.html>`__ - Gets/sets all
   parameters for the resource_broker
-  `drpcli resource_brokers
   patch <drpcli_resource_brokers_patch.html>`__ - Patch resource_broker
   by ID using the passed-in JSON Patch
-  `drpcli resource_brokers
   pause <drpcli_resource_brokers_pause.html>`__ - Mark the
   resource_broker as NOT runnable
-  `drpcli resource_brokers
   processjobs <drpcli_resource_brokers_processjobs.html>`__ - For the
   given resource_broker, process pending jobs until done.
-  `drpcli resource_brokers
   releaseToPool <drpcli_resource_brokers_releaseToPool.html>`__ -
   Release this resource_broker back to the pool
-  `drpcli resource_brokers
   remove <drpcli_resource_brokers_remove.html>`__ - Remove the param
   *key* from resource_brokers
-  `drpcli resource_brokers
   removeprofile <drpcli_resource_brokers_removeprofile.html>`__ -
   Remove a profile from the resource_broker’s profile list
-  `drpcli resource_brokers
   removetask <drpcli_resource_brokers_removetask.html>`__ - Remove a
   task from the resource_broker’s list
-  `drpcli resource_brokers run <drpcli_resource_brokers_run.html>`__ -
   Mark the resource_broker as runnable
-  `drpcli resource_brokers
   runaction <drpcli_resource_brokers_runaction.html>`__ - Run action on
   object from plugin
-  `drpcli resource_brokers set <drpcli_resource_brokers_set.html>`__ -
   Set the resource_brokers param *key* to *blob*
-  `drpcli resource_brokers show <drpcli_resource_brokers_show.html>`__
   - Show a single resource_brokers by id
-  `drpcli resource_brokers
   stage <drpcli_resource_brokers_stage.html>`__ - Set the
   resource_broker’s stage
-  `drpcli resource_brokers
   start <drpcli_resource_brokers_start.html>`__ - Start the resource
   broker’s workflow
-  `drpcli resource_brokers
   tasks <drpcli_resource_brokers_tasks.html>`__ - Access task
   manipulation for resource_brokers
-  `drpcli resource_brokers
   update <drpcli_resource_brokers_update.html>`__ - Unsafely update
   resource_broker by id with the passed-in JSON
-  `drpcli resource_brokers
   uploadiso <drpcli_resource_brokers_uploadiso.html>`__ - This will
   attempt to upload the ISO from the specified ISO URL.
-  `drpcli resource_brokers wait <drpcli_resource_brokers_wait.html>`__
   - Wait for a resource_broker’s field to become a value within a
   number of seconds
-  `drpcli resource_brokers
   whoami <drpcli_resource_brokers_whoami.html>`__ - Figure out what
   resource_broker UUID most closely matches the current system
-  `drpcli resource_brokers
   work_order <drpcli_resource_brokers_work_order.html>`__ - Access
   commands for manipulating the work order queue
-  `drpcli resource_brokers
   workflow <drpcli_resource_brokers_workflow.html>`__ - Set the
   resource_broker’s workflow
