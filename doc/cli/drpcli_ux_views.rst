
.. _rs_drpcli_ux_views:

drpcli ux_views
---------------

Access CLI commands relating to ux_views

Options
~~~~~~~

::

     -h, --help   help for ux_views

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli ux_views action <drpcli_ux_views_action.html>`__ - Display
   the action for this ux_view
-  `drpcli ux_views actions <drpcli_ux_views_actions.html>`__ - Display
   actions for this ux_view
-  `drpcli ux_views await <drpcli_ux_views_await.html>`__ - Wait for a
   ux_view’s field to become a value within a number of seconds
-  `drpcli ux_views count <drpcli_ux_views_count.html>`__ - Count all
   ux_views
-  `drpcli ux_views create <drpcli_ux_views_create.html>`__ - Create a
   new ux_view with the passed-in JSON or string key
-  `drpcli ux_views destroy <drpcli_ux_views_destroy.html>`__ - Destroy
   ux_view by id
-  `drpcli ux_views etag <drpcli_ux_views_etag.html>`__ - Get the etag
   for a ux_views by id
-  `drpcli ux_views exists <drpcli_ux_views_exists.html>`__ - See if a
   ux_views exists by id
-  `drpcli ux_views indexes <drpcli_ux_views_indexes.html>`__ - Get
   indexes for ux_views
-  `drpcli ux_views list <drpcli_ux_views_list.html>`__ - List all
   ux_views
-  `drpcli ux_views patch <drpcli_ux_views_patch.html>`__ - Patch
   ux_view by ID using the passed-in JSON Patch
-  `drpcli ux_views runaction <drpcli_ux_views_runaction.html>`__ - Run
   action on object from plugin
-  `drpcli ux_views show <drpcli_ux_views_show.html>`__ - Show a single
   ux_views by id
-  `drpcli ux_views update <drpcli_ux_views_update.html>`__ - Unsafely
   update ux_view by id with the passed-in JSON
-  `drpcli ux_views wait <drpcli_ux_views_wait.html>`__ - Wait for a
   ux_view’s field to become a value within a number of seconds
