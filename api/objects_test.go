package api

import (
	"testing"
)

func TestObject(t *testing.T) {
	test := &crudTest{
		name: "get objects",
		expectRes: []string{
			"activities",
			"alerts",
			"batches",
			"blueprints",
			"bootenvs",
			"catalog_items",
			"clusters",
			"contexts",
			"endpoints",
			"filters",
			"identity_providers",
			"jobs",
			"kk",
			"leases",
			"machines",
			"params",
			"plugins",
			"pools",
			"preferences",
			"profiles",
			"reservations",
			"resource_brokers",
			"roles",
			"stages",
			"subnets",
			"tasks",
			"templates",
			"tenants",
			"trigger_providers",
			"triggers",
			"users",
			"ux_options",
			"ux_settings",
			"ux_views",
			"version_sets",
			"work_orders",
			"workflows",
		},
		expectErr: nil,
		op: func() (interface{}, error) {
			return session.Objects()
		},
	}
	test.run(t)

}
