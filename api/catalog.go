package api

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitlab.com/rackn/provision/v4/models"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var defaultCatalogUrl = "https://repo.rackn.io"

func FetchCatalogUrls(session *Client, catalogUrlsToFetch string) (catalogUrls []string, err error) {
	switch catalogUrlsToFetch {
	case "cli":
		return // the default catalog will be the only one being used
	case "server":
		if catalogUrlParam, catalogUrlsParam, cerr := fetchServerUrls(session); cerr == nil {
			// Append the server urls
			catalogUrls = append(catalogUrls, catalogUrlParam)
			for i := len(catalogUrlsParam) - 1; i >= 0; i-- {
				catalogUrls = append(catalogUrls, catalogUrlsParam[i])
			}
		} else {
			err = cerr
			return
		}
	case "all":
		if catalogUrlParam, catalogUrlsParam, cerr := fetchServerUrls(session); cerr == nil {
			// Append the server urls
			catalogUrls = append(catalogUrls, catalogUrlParam)
			for i := len(catalogUrlsParam) - 1; i >= 0; i-- {
				catalogUrls = append(catalogUrls, catalogUrlsParam[i])
			}
		} // Ignore if there was an error, we want to move on and retrieve other urls as well
		// Also append default url
		catalogUrls = append(catalogUrls, defaultCatalogUrl)
	default:
		err = fmt.Errorf("invalid value received for catalogs to fetch %s. Allowed values are 'cli' 'server' or 'all'", catalogUrlsToFetch)
		return
	}

	for i, cUrl := range removeEmptyFromSlice(catalogUrls) {
		if session != nil {
			info, _ := session.Info()
			catalogUrls[i] = FixSource(cUrl, fmt.Sprintf("http://%s", net.JoinHostPort(session.Host(), strconv.Itoa(info.FilePort))))
		}
	}

	return
}

func fetchServerUrls(session *Client) (catalogUrlParam string, catalogUrlsParam []string, err error) {
	if session == nil {
		err = fmt.Errorf("A session is required to fetch server urls")
		return
	}
	// Naming them the same as the params for better understanding
	if lerr := session.Req().UrlFor("profiles", "global", "params", "catalog_url").Do(&catalogUrlParam); lerr != nil {
		err = fmt.Errorf("failed to get catalog_url: %v", lerr)
		return
	}

	if req := session.Req().UrlFor("profiles", "global", "params", "catalog_urls"); req != nil {
		if lerr := req.Do(&catalogUrlsParam); lerr != nil {
			err = fmt.Errorf("failed to get catalog_urls: %v", lerr)
			return
		}
	}
	return
}

func getUrl(source string) ([]byte, error) {
	if u, err := url.Parse(source); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}
		res, err := client.Get(source)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		return []byte(body), err
	} else if err == nil && u.Scheme == "file" {
		source = filepath.Join(u.Host, u.Path)
		if s, err := os.Lstat(source); err == nil && s.Mode().IsRegular() {
			return ioutil.ReadFile(source)
		}
	}
	return []byte(source), nil
}

func GetCatalogSource(catalogUrl string, session *Client) (catalogSource string) {
	if u, err := url.Parse(catalogUrl); err == nil {
		if u.Scheme == "http" || u.Scheme == "https" {
			catalogSource = fmt.Sprintf("%s://%s/files/rebar-catalog", u.Scheme, u.Host)
		} else if u.Scheme == "file" {
			catalogSource = catalogUrl[:strings.LastIndex(catalogUrl, "/")]
		} else if u.Scheme == "" {
			if session != nil {
				info, _ := session.Info()
				catalogSource = fmt.Sprintf("http://%s/files/rebar-catalog", net.JoinHostPort(session.Host(), strconv.Itoa(info.FilePort)))
			} else {
				catalogSource = ""
			}
		}
	}
	return
}

func FixSource(source, catalogSource string) string {
	// Replace ProvisionerUrl
	provisionerUrlRegex := regexp.MustCompile(`{{\s*.ProvisionerURL\s*}}`)
	source = provisionerUrlRegex.ReplaceAllString(source, catalogSource)
	return source
}

// GetCatalog retrieves contents from the given catalog url
// NOTE: if logic here is updated - please also look in cli/utils.go to update similar logic there
// TODO: update so this is common between api and cli packages
func GetCatalog(catalog string, session *Client) (res *models.Content, err error) {
	buf := []byte{}
	buf, err = getUrl(catalog)
	if err == nil {
		err = json.Unmarshal(buf, &res)
	}
	if err != nil {
		err = fmt.Errorf("error fetching catalog: %v", err)
	}

	catalogSource := GetCatalogSource(catalog, session)

	// Process catalog items to fix their source
	ProcessCatalog(catalog, catalogSource, res)
	return
}

func ProcessCatalog(catalog, catalogSource string, res *models.Content) {
	// Process catalog items to fix their source
	if res != nil {
		for k, v := range res.Sections["catalog_items"] {
			item := &models.CatalogItem{}
			if err := models.Remarshal(v, &item); err != nil {
				continue
			}

			// Fix the source section of the catalog items
			// 1. Replace ProvisionerUrl if it exists
			item.Source = FixSource(item.Source, catalog)

			// If it is a valid url leave it be
			// If absolute path, then file://
			// If relative path, then Source of Catalog/<path>
			if u, err := url.Parse(item.Source); err == nil {
				if u.Scheme == "" {
					if cs, err := url.Parse(catalogSource); err == nil {
						if cs.Scheme != "" {
							cs.Path = filepath.Join(cs.Path, item.Source)
							item.Source = cs.String()
						} else {
							item.Source = filepath.Join(catalogSource, item.Source)
						}
					} else {
						item.Source = filepath.Join(catalogSource, item.Source)
					}
				}
			}
			res.Sections["catalog_items"][k] = item
		}
	}
	return
}

func CombineCatalogs(catalogs []*models.Content) (res *models.Content, err error) {
	result := &models.Content{}
	result.Fill()
	result.Sections["catalog_items"] = make(map[string]interface{})
	for _, oneCatalog := range catalogs {
		for k, v := range oneCatalog.Sections["catalog_items"] {
			result.Sections["catalog_items"][k] = v
		}
	}
	resBuf, _ := json.Marshal(result)
	err = json.Unmarshal(resBuf, &res)

	return
}

// FetchCatalog fetches contents from all catalog urls
// NOTE: if logic here is updated - please also look in cli/utils.go to update similar logic there
// TODO: update so this is common between api and cli packages
func FetchCatalog(catalogUrls []string, session *Client) (res *models.Content, err error) {
	var catalogs []*models.Content
	// Loop through all urls and gather all the content
	for _, cUrl := range catalogUrls {
		content, _ := GetCatalog(cUrl, session)
		if content != nil {
			catalogs = append(catalogs, content)
		}
	}

	res, err = CombineCatalogs(catalogs)
	return
}
