{
  "$defs": {
    "Claim": {
      "additionalProperties": false,
      "description": "Claim is an individial specifier for something we are allowed access to.",
      "properties": {
        "action": {
          "type": "string"
        },
        "scope": {
          "type": "string"
        },
        "specific": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Task": {
      "additionalProperties": false,
      "description": "Task is a thing that can run on a Machine.",
      "properties": {
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Description": {
          "description": "Description is a one-line description of this Task.",
          "type": "string"
        },
        "Documentation": {
          "description": "Documentation should describe in detail what this task should do on a machine.",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ExtraClaims": {
          "description": "ExtraClaims is a raw list of Claims that should be added to the default\nset of allowable Claims when a Job based on this task is running.\nAny extra claims added here will be added _after_ any added by ExtraRoles",
          "items": {
            "$ref": "#/$defs/Claim"
          },
          "type": "array"
        },
        "ExtraRoles": {
          "description": "ExtraRoles is a list of Roles whose Claims should be added to the default\nset of allowable Claims when a Job based on this task is running.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "Name is the name of this Task.  Task names must be globally unique\n\nrequired: true",
          "type": "string"
        },
        "OptionalParams": {
          "description": "OptionalParams are extra optional parameters that a template rendered for\nthe Task may use.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "OutputParams": {
          "description": "OutputParams are that parameters that are possibly set by the Task",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Prerequisites": {
          "description": "Prerequisites are tasks that must have been run in the current\nBootEnv before this task can be run.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "RequiredParams": {
          "description": "RequiredParams is the list of parameters that are required to be present on\nMachine.Params or in a profile attached to the machine.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Templates": {
          "description": "Templates lists the templates that need to be rendered for the Task.\n\nrequired: true",
          "items": {
            "$ref": "#/$defs/TemplateInfo"
          },
          "type": "array"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    },
    "TemplateInfo": {
      "additionalProperties": false,
      "description": "TemplateInfo holds information on the templates in the boot environment that will be expanded into files.",
      "properties": {
        "Contents": {
          "description": "Contents that should be used when this template needs\nto be expanded.  Either this or ID should be set.\n\nrequired: false",
          "type": "string"
        },
        "ID": {
          "description": "ID of the template that should be expanded.  Either\nthis or Contents should be set\n\nrequired: false",
          "type": "string"
        },
        "Link": {
          "description": "Link optionally references another file to put at\nthe path location.",
          "type": "string"
        },
        "Meta": {
          "description": "Meta for the TemplateInfo.  This can be used by the job running\nsystem and the bootenvs to handle OS, arch, and firmware differences.\n\nrequired: false",
          "patternProperties": {
            ".*": {
              "type": "string"
            }
          },
          "type": "object"
        },
        "Name": {
          "description": "Name of the template\n\nrequired: true",
          "type": "string"
        },
        "Path": {
          "description": "A text/template that specifies how to create\nthe final path the template should be\nwritten to.\n\nrequired: true",
          "type": "string"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/task",
  "$ref": "#/$defs/Task",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
